<?php
/**
 * Created by PhpStorm.
 * User: kushagra
 * Date: 14/08/17
 * Time: 11:21 PM
 */

namespace ConnectInn\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleNotFoundException extends NotFoundHttpException
{
    const MESSAGE = 'Requested Article not found!';
    const CODE = 2;

    public function __construct()
    {
        parent::__construct(self::MESSAGE, null, self::CODE);
    }
}