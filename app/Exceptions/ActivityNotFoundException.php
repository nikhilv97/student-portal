<?php
/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 14/6/17
 * Time: 3:53 PM
 */

namespace ConnectInn\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ActivityNotFoundException extends NotFoundHttpException
{
    const MESSAGE = "Requested activity is not found";
    const CODE = 1;

    public function __construct()
    {
        parent::__construct(self::MESSAGE, null, self::CODE);
    }


}