<?php
/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 14/6/17
 * Time: 3:26 PM
 */

namespace ConnectInn\Exceptions;



use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserNotFoundException extends NotFoundHttpException
{
    const MESSAGE = 'Requested user could not be found!';
    const CODE = 0;

    public function __construct()
    {
        parent::__construct(self::MESSAGE,  null, self::CODE);

    }
}