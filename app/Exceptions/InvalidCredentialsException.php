<?php
/**
 * Created by PhpStorm.
 * User: kushagra
 * Date: 01/08/17
 * Time: 10:18 PM
 */

namespace ConnectInn\Exceptions;


use Lang;
use Symfony\Component\HttpKernel\Exception\HttpException;

class InvalidCredentialsException extends HttpException
{
    public function __construct()
    {
        parent::__construct(422, Lang::get('auth.failed'));
    }
}