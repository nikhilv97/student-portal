<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 10/4/18
 * Time: 5:02 PM
 */

namespace ConnectInn\Exceptions;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ForumNotFoundException extends NotFoundHttpException
{
    const MESSAGE = "Requested Forum is not found";
    const CODE = 3;

    public function __construct()
    {
        parent::__construct(self::MESSAGE, null, self::CODE);
    }

}