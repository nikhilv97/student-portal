<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;


/**
 * ConnectInn\Activity
 *
 * @property int $id
 * @property int|null $user_document_id
 * @property string $title
 * @property string|null $desc
 * @property string $start
 * @property string|null $end
 * @property string|null $external_link
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Milestone[] $milestones
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\User[] $users
 * @method static Builder|Activity whereCreatedAt($value)
 * @method static Builder|Activity whereDesc($value)
 * @method static Builder|Activity whereEnd($value)
 * @method static Builder|Activity whereExternalLink($value)
 * @method static Builder|Activity whereId($value)
 * @method static Builder|Activity whereStart($value)
 * @method static Builder|Activity whereTitle($value)
 * @method static Builder|Activity whereType($value)
 * @method static Builder|Activity whereUpdatedAt($value)
 * @method static Builder|Activity whereUserDocumentId($value)
 * @mixin \Eloquent
 * @property string|null $link
 * @method static Builder|Activity whereLink($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\UserDocument[] $documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\ActivityRequest[] $activityRequests
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Tag[] $tags
 */
class Activity extends Model
{
    use Searchable;


    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $tags = $this->tags->toArray();
        $array['tags'] = $tags;

        return $array;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'activity_user')
            ->withPivot('rating', 'working_as', 'type', 'number_of_ratings')->withTimestamps();
    }

    public function milestones()
    {
        return $this->hasMany(Milestone::class)->orderBy('created_at', 'desc');
    }

    public function owner()
    {
        return $this->users()->where('type', 'owner')->first();
    }

    public function members(){
        return $this->users()->where('type', 'member')->get();

    }

    public function documents(){
        return $this->hasMany(UserDocument::class);
    }

    public function activityRequests(){
        return $this->hasMany(ActivityRequest::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class, 'activity_tags');
    }
}
