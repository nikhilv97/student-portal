<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttributeAddRequest extends FormRequest
{

    const VALUE = 'value';
    const TYPE = 'type';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::VALUE => 'required',
            self::TYPE =>  'required|in:skill,interest'
        ];
    }

    public function getType(){
        return $this->get(self::TYPE);

    }

    public function getValue(){
        return $this->get(self::VALUE);
    }
}
