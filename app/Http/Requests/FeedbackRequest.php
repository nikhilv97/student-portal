<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    const NAME = 'name';
    const EMAIL = 'email';
    const MESSAGE = 'message';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::NAME =>'required',
            self::EMAIL=>'required',
            self::MESSAGE =>'required'
        ];
    }

    public function getName(){
        return $this->get(self::NAME);
    }

    public function getEmail(){
        return $this->get(self::EMAIL);
    }

    public function getMessage(){
        return $this->get(self::MESSAGE);
    }
}
