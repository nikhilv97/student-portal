<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'start' => 'required|date',
            'end' => 'date|after:start',
            'type' => 'required|in:project,competition,seminar,workshop,guest_lecture,co_curricular,certification,training,other,volunteer,publication',
            'link' => 'active_url',
            'external_link' => 'active_url'
        ];
    }
}
