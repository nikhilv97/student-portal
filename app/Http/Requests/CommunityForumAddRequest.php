<?php
/**
 * Created by PhpStorm.
 * User: nik
 * Date: 6/4/18
 * Time: 9:39 PM
 */

namespace ConnectInn\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CommunityForumAddRequest extends FormRequest
{
    CONST TITLE = 'title';
    CONST ISPUBLIC = 'isPublic';
    CONST DESC = 'description';

    public function authorize()
    {
        return true;
    }

    public function rules(){

        return [
            self::TITLE => 'required',
            self::ISPUBLIC =>'required',
        ];

    }
}