<?php

namespace ConnectInn\Http\Requests;

use ConnectInn\Activity;
use ConnectInn\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ActivityDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('activity_id');
        $activity = Activity::find($id);
        foreach ($activity->users as $user) {
            if (Auth::id() == $user->id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
