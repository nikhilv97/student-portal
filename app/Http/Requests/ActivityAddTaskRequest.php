<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityAddTaskRequest extends FormRequest
{


    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const EMAIL = 'email';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::TITLE => 'required',
            self::DESCRIPTION => 'required',
            self::EMAIL => 'required|email|exists:users,email'
        ];
    }

    public function getTitle(){
        return $this->get(self::TITLE);

    }

    public function getDescription(){
        return $this->get(self::DESCRIPTION);

    }

    public function getType(){
        return 'task';

    }

    public function getStatus(){
        return 'created';

    }

    public function getEmail(){
        return $this->get(self::EMAIL);
    }

}
