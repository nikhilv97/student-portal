<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 10/4/18
 * Time: 7:10 PM
 */

namespace ConnectInn\Http\Requests;


use function foo\func;
use Illuminate\Foundation\Http\FormRequest;

class CommunityForumRemoveUserRequest extends FormRequest
{
    CONST EMAIL = 'email';


    public function authorize()
    {
          return true;
    }

    public function rules()
    {
        return [
            self::EMAIL => 'required|email'
        ];
    }
}