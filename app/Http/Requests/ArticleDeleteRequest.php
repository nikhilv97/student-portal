<?php

namespace ConnectInn\Http\Requests;

use Auth;
use ConnectInn\Services\ArticleService;
use Illuminate\Foundation\Http\FormRequest;

class ArticleDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $article = $this->route('article');

        return $article->user_id === Auth::id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
