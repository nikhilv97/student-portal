<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EducationAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    const NAME = 'name';
    const DEGREE = 'degree';
    const GRADE = 'grade';
    const GRADE_TYPE = 'grade_type';
    const FROM = 'from';
    const TO = 'to';
    const LOCATION = 'location';
    const OFFICIAL_BODY = 'official_body';


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::NAME => 'required',
            self::DEGREE => 'required',
            self::GRADE => 'required|integer|min:0|max:100',
            self::GRADE_TYPE => 'required|in:cgpa,percentage',
            self::FROM => 'required|integer',
            self::TO => 'integer',
            self::LOCATION => 'required',
            self::OFFICIAL_BODY => 'required'
        ];
    }

    public function getName(){
        return $this->get(self::NAME);
    }

    public function getDegree(){
        return $this->get(self::DEGREE);
    }

    public function getGrade(){
        return $this->get(self::GRADE);
    }

    public function getGradeType(){
        return $this->get(self::GRADE_TYPE);
    }

    public function getFrom(){
        return $this->get(self::FROM);
    }

    public function getOfficialBody(){
        return $this->get(self::OFFICIAL_BODY);
    }

    public function hasTo(){
        return $this->has(self::TO);
    }

    public function getTo(){
        return $this->get(self::TO);
    }

    public function getLocation(){
        return $this->get(self::LOCATION);
    }
}
