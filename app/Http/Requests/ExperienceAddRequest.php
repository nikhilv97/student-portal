<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExperienceAddRequest extends FormRequest
{
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const DESIGNATION = 'designation';
    const  LOCATION = 'location';
    const FROM = 'from';
    const TO = 'to';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::NAME => 'required',
            self::LOCATION => 'required',
            self::DESIGNATION => 'required',
            self::FROM => 'required|integer',
            self::TO => 'integer'
        ];
    }

    public function getName(){
        return $this->get(self::NAME);
    }

    public function getDescription(){
        return $this->get(self::DESCRIPTION);
    }

    public function getDesignation(){
        return $this->get(self::DESIGNATION);
    }

    public function hasDesignation(){
        return $this->has(self::DESIGNATION);
    }


    public function getFrom(){
        return $this->get(self::FROM);
    }


    public function hasTo(){
        return $this->has(self::TO);
    }

    public function getTo(){
        return $this->get(self::TO);
    }

    public function getLocation(){
        return $this->get(self::LOCATION);
    }
}
