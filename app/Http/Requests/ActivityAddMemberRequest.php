<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityAddMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     *
     */

    const EMAIL = 'email';
    const WORKINGAS = 'workingAs';


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::EMAIL => 'required|exists:users,email'
        ];
    }

    public function getEmail(){
        return $this->get(self::EMAIL);

    }

    public function hasWorkingAs() {
        return $this->has(self::WORKINGAS);
    }

    public function getWorkingAs(){
        return $this->get(self::WORKINGAS);
    }
}
