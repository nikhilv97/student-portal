<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentAddRequest extends FormRequest
{

    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const FILE  = 'file';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::TITLE => 'required',
            self::FILE => 'required'
        ];
    }

    public function getTitle(){
        return $this->get(self::TITLE);
    }

    public function getFile(){
        return $this->file(self::FILE);
    }

    public function hasDescription(){
        return $this->has(self::DESCRIPTION);
    }

    public function getDescription(){
        return $this->get(self::DESCRIPTION);
    }
}
