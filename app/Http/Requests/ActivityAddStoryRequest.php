<?php

namespace ConnectInn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityAddStoryRequest extends FormRequest
{



    const TITLE = 'title';
    const DESCRIPTION = 'description';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            self::TITLE => 'required',
            self::DESCRIPTION => 'required'
        ];
    }

    public function getTitle(){
        return $this->get(self::TITLE);

    }

    public function getDescription(){
        return $this->get(self::DESCRIPTION);

    }

    public function getType(){
        return 'story';

    }

    public function getStatus(){
        return 'created';

    }
}
