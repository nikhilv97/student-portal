<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 10/4/18
 * Time: 5:13 PM
 */

namespace ConnectInn\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CommunityForumAddUserRequest extends FormRequest
{

    CONST EMAILS = 'email';


    public function __construct(){

        Validator::extend("emails", function($attribute, $value, $parameters) {
            $rules = [
                'email' => 'required|exists:users,email'
            ];
            foreach ($value as $email) {
                $data = [
                    'email' => $email
                ];
                $validator = Validator::make($data, $rules);
                if ($validator->fails()) {
                    return false;
                }
            }
            return true;
        });
    }

    public function authorize()
    {
        return true;
    }

    public function rules() {
        return [
           'email'=> 'emails'
        ];
    }

}