<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Activity;
use ConnectInn\Http\Requests\ActivityAddStoryRequest;
use ConnectInn\Http\Requests\ActivityAddTaskRequest;
use ConnectInn\Jobs\SendStoryAddedEmailJob;
use ConnectInn\Jobs\SendTaskAssignedEmailJob;
use ConnectInn\Services\MilestoneService;
use ConnectInn\User;
use Illuminate\Support\Facades\Redirect;

class ActivityMilestoneController extends Controller
{
    protected $milestoneService;

    public function __construct()
    {
        $this->milestoneService = new MilestoneService();
    }

    public function saveStory(ActivityAddStoryRequest $request, Activity $activity)
    {
         $story = $this->milestoneService->saveStory($request, $activity);

        foreach ($activity->members() as $member) {

            $this->dispatch(new SendStoryAddedEmailJob($activity, $member));

         }

         return view('activities.milestones.story', [
             'story' => $story
         ]);

    }

    public function saveTask(ActivityAddTaskRequest $request, Activity $activity)
    {
        $task = $this->milestoneService->saveTask($request, $activity);

        $userEmail = $request->getEmail();
        $user = User::where('email' , $userEmail)->first();
        $this->dispatch(new SendTaskAssignedEmailJob($activity, $user));


        return view('activities.milestones.task', [
            'task' => $task
        ]);
    }
}
