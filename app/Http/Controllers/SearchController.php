<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Http\Requests\UserSerchRequest;
use ConnectInn\User;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(UserSerchRequest $request)
    {
        $search_string = $request->get('query');

        $users = User::search($search_string)->orderBy('name')->get();

        return view('result', compact('users'))
            ->with('query', $request->get('query'));

    }
}
