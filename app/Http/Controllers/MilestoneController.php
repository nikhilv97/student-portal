<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Milestone;
use ConnectInn\Services\MilestoneService;
use Illuminate\Http\Request;

class MilestoneController extends Controller
{
    protected $milestoneService;

    public function __construct()
    {
        $this->milestoneService = new MilestoneService();
    }


    function update(Request $request, Milestone $milestone)
    {
        $this->validate($request, [
            'attribute' => 'required|in:status',
            'value' => 'required'
        ]);
        $attribute = $request->get('attribute');
        $value = $request->get('value');

        $this->milestoneService->update($milestone, $attribute, $value);

        return $milestone->status;
    }
}
