<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Experience;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    public function destroy(Experience $experience) {
        $experience->delete();
    }
}
