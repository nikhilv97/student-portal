<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Attribute;
use ConnectInn\Education;
use ConnectInn\Exceptions\UserNotFoundException;
use ConnectInn\Experience;
use ConnectInn\Http\Requests\AttributeAddRequest;
use ConnectInn\Http\Requests\DocumentAddRequest;
use ConnectInn\Http\Requests\EducationAddRequest;
use ConnectInn\Http\Requests\ExperienceAddRequest;
use ConnectInn\Http\Requests\ImageUploadRequest;
use ConnectInn\Http\Requests\UserUpdateRequest;
use ConnectInn\Services\ImageService;
use ConnectInn\Services\UserDocumentService;
use ConnectInn\Services\UserService;
use ConnectInn\Services\ValidatorService;
use ConnectInn\Tag;
use ConnectInn\UserDocument;
use Curl\Curl;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class UserController extends Controller
{
    protected $userService;
    protected $userDocumentService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->userDocumentService = new UserDocumentService();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $tags = Tag::all();
        return view(('users.profile'), [
            'user' => $user,
            'tags' => $tags
        ]);
    }

    /**
     * @param UserUpdateRequest $request
     * @return mixed
     */
    public function update(UserUpdateRequest $request)
    {
        $this->userService->updateUser($request);

        if ($request->get('name') === 'name') {
            return $request->get('value');
        }

        return Redirect::back();


    }


    /**
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfile($user)
    {
        $tags = Tag::all();
        try {
            $user = $this->userService->getUserForId($user);
        } catch (UserNotFoundException $e) {
            return view('deadLink', [
                'message' => $e->getMessage()
            ]);
        }

        return view('users.profile', [
            'user' => $user,
            'tags' => $tags
        ]);


    }

    /**
     * @param ImageUploadRequest $request
     * @param ImageService $service
     * @return Redirect
     */
    public function profilePic(ImageUploadRequest $request, ImageService $service)
    {
        $url = $service->uploadImage($request);

        $user = Auth::user();
        $user->profile_pic_url = $url;
        $user->save();

        return redirect(url('/myprofile'));
    }

    public function addFollower($user)
    {
        $this->userService->attachUser($user);

        return Redirect::back();


    }

    public function removeFollower($user)
    {
        $this->userService->detachUser($user);

        return Redirect::back();


    }

    public function fetchFollowers()
    {
        $users = $this->userService->getFollowers();

        return view('users.followers', compact('users'));


    }

    public function downloadResume()
    {
        $user = Auth::user();

        $dompdf = new Dompdf();
        $json = UserService::resumeJson();

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setOpt(CURLOPT_URL, 'https://themes.jsonresume.org/stackoverflow');
        $curl->setOpt(CURLOPT_POSTFIELDS, \GuzzleHttp\json_encode($json));
        $curl->exec();

        if ($curl->httpStatusCode === 200) {
            $user->resume_html = $curl->rawResponse;

            if (is_null($user->hash_code)) {
                $user->hash_code = md5($user->email);
            }
            $user->save();
        }

        return $curl->rawResponse;

//        $dompdf->loadHtml($curl->rawResponse);
//
//        $dompdf->render();
//
//        $dompdf->stream($user->name);

    }

    public function addEducation(EducationAddRequest $request)
    {
        $education = $this->userService->addEducation($request);

        if (Auth::user()->educations->count() === 1) {
            return view('users.educations.educationList', [
                'educations' => Auth::user()->educations,
                'user' => Auth::user()
            ]);
        } else {
            return view('users.educations.educationListItem', [
                'education' => $education,
                'user' => Auth::user()
            ]);
        }
    }

    public function addExperience(ExperienceAddRequest $request)
    {
        $experience = $this->userService->addExperience($request);

        if (Auth::user()->experiences->count() === 1) {
            return view('users.experiences.experienceList', [
                'experiences' => Auth::user()->experiences,
                'user' => Auth::user()
            ]);
        } else {
            return view('users.experiences.experienceListItem', [
                'experience' => $experience,
                'user' => Auth::user()
            ]);
        }
    }

    public function addAttribute(AttributeAddRequest $request)
    {
        $user = Auth::user();
        $attribute = $this->userService->addAttribute($request);

        if ($request->getType() === 'skill') {
            if ($user->skills()->count() === 1) {
                return view('users.skills.skillList', [
                    'user' => $user,
                    'skills' => $user->skills()
                ]);
            } else {
                return view('users.skills.skillListItem', [
                    'skill' => $attribute,
                    'user' => Auth::user()
                ]);
            }
        } else {
            if ($user->interests()->count() === 1) {
                return view('users.interests.interestList', [
                    'user' => $user,
                    'interests' => $user->interests()
                ]);
            } else {
                return view('users.interests.interestListItem', [
                    'interest' => $attribute,
                    'user' => Auth::user()]);
            }
        }
    }

    public
    function addDocument(DocumentAddRequest $request)
    {
        $this->userDocumentService->addDocument($request);

        return Redirect::back();
    }

    public
    function addEducationDocument(DocumentAddRequest $request, Education $education)
    {
        $this->userDocumentService->addEducationDocument($request, $education);

        return Redirect::back();
    }

    public
    function addExperienceDocument(DocumentAddRequest $request, Experience $experience)
    {
        $this->userDocumentService->addExperienceDocument($request, $experience);

        return Redirect::back();
    }

    public
    function deleteAttribute(Attribute $attribute)
    {
        $attribute->delete();
    }

    public
    function deleteExperience(Experience $experience)
    {
        $experience->delete();

    }

    public
    function deleteEducation(Education $education)
    {
        $education->delete();
    }

    public
    function deleteDocument(UserDocument $document)
    {
        $document->delete();
    }

    public
    function addLanguage(Request $request)
    {
        $this->validate($request, ValidatorService::languageValidator());

        $user = Auth::user();
        $data = [
            [
                'id' => uniqid('lang'),
                'language' => $request->get('language'),
                'fluency' => $request->get('fluency')
            ]
        ];
        //dd($user->languages);

        if (!$user->languages) {
            $user->languages = json_encode($data);
        } else {
            $user->languages = json_encode(array_merge(json_decode($user->languages, true), $data));
        }

        $user->save();
        //dd($user->languages);
        return Redirect::back();
    }
}
