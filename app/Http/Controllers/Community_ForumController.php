<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Community_Forums;
use ConnectInn\Http\Requests\CommunityForumAddRequest;
use ConnectInn\Http\Requests\CommunityForumAddUserRequest;
use ConnectInn\Http\Requests\CommunityForumRemoveUserRequest;
use ConnectInn\Services\CommunityForumService;
use Illuminate\Http\Request;

class Community_ForumController extends Controller
{
    //
    protected  $communityforumService;
    public function __construct()
    {
        $this->communityforumService = new CommunityForumService();
    }
    public function index(Request $request){
        $forums = Community_Forums::paginate(10);
        return view('forum.forumsList',compact('forums'));

    }

    public function createNewForum(CommunityForumAddRequest $request){

         $forum = $this->communityforumService->create($request);

         return $forum;
     }

     public function deleteForum(Request $request,$id){
        $forum = $this->communityforumService->delete($request,$id);

         return Redirect::back();
     }


     public function liveForum(Request $request,$id){
        dd('forum number  '.$id);
         return CommunityForumService::find($id);
     }


     public function addUsers(CommunityForumAddUserRequest $request, $id){

          $users = $this->communityforumService->addUsers($request,$id);

          return $users;
     }

     public function removeUser(CommunityForumRemoveUserRequest $request,$id){

         $user = $this->communityforumService->removeUser($request,$id);

         return $user;
     }




}
