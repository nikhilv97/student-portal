<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Education;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function destroy(Education $education) {
        $education->delete();
    }
}
