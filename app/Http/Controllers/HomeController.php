<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Activity;
use ConnectInn\Article;
use ConnectInn\Http\Requests\FeedbackRequest;
use ConnectInn\Jobs\ReceiveFeedbackEmail;
use ConnectInn\Jobs\ReceiveFeedbackEmailJob;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }


    public function showHomePage()
    {
        if (Auth::check()) {
            $articles = Article::with('likes')->orderBy('created_at', 'desc')->paginate(10);

            return view('home', compact('articles'));

        } else {
            return redirect(url('/login'));
        }
    }

    public function writeUs(FeedbackRequest $request){
      $name = $request->getName();
      $email = $request->getEmail();
      $message = $request->getMessage();

        $this->dispatch(new ReceiveFeedbackEmailJob($name , $email , $message));

    }


}
