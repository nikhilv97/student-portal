<?php

namespace ConnectInn\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    //
    public function home()
    {
        if (Auth::check()) {
            return redirect(url('/home'));
        }

        return view('welcome');
    }
}
