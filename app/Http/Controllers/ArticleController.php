<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Article;
use ConnectInn\Http\Requests\ArticleCreateRequest;
use ConnectInn\Http\Requests\ArticleDeleteRequest;
use ConnectInn\Like;
use ConnectInn\Services\ArticleService;
use ConnectInn\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use DB;
use phpDocumentor\Reflection\Types\Null_;

class ArticleController extends Controller
{
    private $articleService;

    public function __construct()
    {
        $this->articleService = new ArticleService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleCreateRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleCreateRequest $request)
    {
        $this->articleService->save($request);


        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ArticleDeleteRequest $request
     * @param Article $article
     * @internal param int $id
     */
    public function destroy(ArticleDeleteRequest $request, Article $article)
    {
        $article->delete();

        return Redirect::back();
    }

    public function like(Request $request,$id)
    {
        $user = Auth::user();
        $article = Article::find($id);

        $already_liked = DB::table('likes')->where([
            ['user_id','=',$user->id],
            ['article_id','=',$id],
        ])->get();

        if($already_liked->isEmpty()){
            $like = new Like();
            $like->user_id = $user->id;
            $like->article_id = $article->id;
            $like->like = 1;
            $like->save();

            return 'likesaved';
        }
        else{
           return 'already liked';
        }


    }


}
