<?php

namespace ConnectInn\Http\Controllers;

use ConnectInn\Activity;
use ConnectInn\ActivityRequest;
use ConnectInn\Experience;
use ConnectInn\Http\Requests\ActivityAddMemberRequest;
use ConnectInn\Http\Requests\ActivityCreateRequest;
use ConnectInn\Http\Requests\ActivityDeleteRequest;
use ConnectInn\Http\Requests\ActivityUpdateRequest;
use ConnectInn\Http\Requests\DocumentAddRequest;
use ConnectInn\Http\Requests\UpadteRatingRequest;
use ConnectInn\Jobs\SendAddMemberEmailJob;
use ConnectInn\Services\ActivityService;
use ConnectInn\Services\UserDocumentService;
use ConnectInn\Services\UserService;
use ConnectInn\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ActivityController extends Controller
{
    protected $activityService;
    protected $userDocumentService;

    public function __construct()
    {
        $this->activityService = new ActivityService();
        $this->userDocumentService = new UserDocumentService();
    }


    public function index(){

        $activities = Activity::paginate(10);
        return view('activities.activitiesList', compact('activities'));
    }




    public function createNewActivity(ActivityCreateRequest $request)
    {
        $activity = $this->activityService->create($request);

        if (Auth::user()->activities->count() === 1) {
            return view('activities.activityList', [
                'activities' => Auth::user()->activities,
                'user' => Auth::user()
            ]);
        } else {
            return view('activities.activityListItem', [
                'activity' => $activity
            ]);
        }
    }

    public function delete(ActivityDeleteRequest $request, $activity_id)
    {
        $this->activityService->delete($activity_id);


        return Redirect::back();
    }

    public function update(ActivityUpdateRequest $request, $activity)
    {
        $this->activityService->update($request, $activity);
    }

    public function show($id)
    {
        try {
            $activity = ActivityService::find($id);
        } catch (\Exception $e) {
            return view('deadLink', [
                'message' => $e->getMessage()
            ]);
        }

        return view('activities.activity', compact('activity'));
    }

    public function addMember(ActivityAddMemberRequest $request, $id)
    {
        $this->activityService->addMember($request, $id);
        $activity = Activity::find($id);

        $userEmail = $request->getEmail();
        $user = User::where('email', $userEmail)->first();
        $this->dispatch(new SendAddMemberEmailJob($activity, $user));


        return Redirect::back();

    }

    public function removeMember($id, $user_id)
    {
        $this->activityService->detachMember($id, $user_id);

        return Redirect::back();
    }

    public function updateRating(UpadteRatingRequest $request, Activity $activity, User $user)
    {
        $this->activityService->updateRating($request, $activity, $user);

        return Redirect::back();

    }

    public function addActivityDocument(DocumentAddRequest $request, Activity $activity)
    {
        $this->userDocumentService->addActivityDocument($request, $activity);

        return Redirect::back();
    }

    public function sendConnectToActivityRequest(Activity $activity)
    {
        $this->activityService->sendConnectRequest($activity);

        return Redirect::back();
    }

    public function acceptRequest($activityRequest)
    {
        $this->activityService->acceptRequest($activityRequest);

    }

    public function rejectRequest($activityRequest)
    {
        $this->activityService->rejectRequest($activityRequest);
    }

    public function search(Request $request) {
        $activities = Activity::search($request->get('query'))->paginate(10);

        return view('activities.activitiesList', compact('activities'));
    }
}
