<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\UserDocument
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Education[] $educations
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Experience[] $experiences
 * @property-read \ConnectInn\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereUserId($value)
 * @mixin \Eloquent
 * @property int|null $activity_id
 * @property int|null $experience_id
 * @property int|null $education_id
 * @property-read \ConnectInn\Activity $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereEducationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\UserDocument whereExperienceId($value)
 */
class UserDocument extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function educations(){
        return $this->belongsTo(Education::class);
    }

    public function experiences(){
        return $this->belongsTo(Experience::class);
    }

    public function activities(){
        return $this->belongsTo(Activity::class);
    }
}
