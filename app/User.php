<?php

namespace ConnectInn;

use Auth;
use ConnectInn\Services\UserService;
use DB;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;


/**
 * ConnectInn\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $branch
 * @property int|null $year_of_passing
 * @property int|null $avg_rating
 * @property string|null $profile_pic_url
 * @property string|null $remember_token
 * @property string|null $hash_code
 * @property string|null $resume_html
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Attribute[] $attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\UserDocument[] $documents
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Education[] $educations
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Experience[] $experiences
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $followers
 * @property-read mixed $interests
 * @property-read mixed $skills
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static Builder|User whereAvgRating($value)
 * @method static Builder|User whereBranch($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereProfilePicUrl($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereYearOfPassing($value)
 * @mixin \Eloquent
 * @property string|null $dob
 * @property string|null $about
 * @method static Builder|User whereAbout($value)
 * @method static Builder|User whereDob($value)
 */
class User extends Authenticatable implements CanResetPassword
{
    use Notifiable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'branch', 'year_of_passing'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        $skills = $this->skills();
        $skillValues = [];

        foreach ($skills as $key => $skill) {
            $skillValues[$key] = $skill->value;
        }

        $interests = $this->interests();
        $interestValues = [];

        foreach ($interests as $key => $interest) {
            $interestValues[$key] = $interest->value;
        }

        $array['skills'] = $skillValues;
        $array['interests'] = $interestValues;

        return $array;
    }


    public function activities()
    {
        return $this->belongsToMany(Activity::class, 'activity_user')
            ->withPivot('rating', 'working_as', 'type', 'number_of_ratings')->withTimestamps();
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id')->withTimestamps()->orderby('created_at', 'desc');
    }

    public function educations()
    {
        return $this->hasMany(Education::class)
            ->orderby('from', 'desc');
    }

    public function experiences()
    {
        return $this->hasMany(Experience::class)
            ->orderBy('from', 'desc');
    }

    public function documents()
    {
        return $this->hasMany(UserDocument::class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

    public function articles() {
        return $this->hasMany(Article::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function skills()
    {
        $skills = $this->attributes()->where('type', 'skill')->get();

        return $skills;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function interests()
    {
        $interests = $this->attributes()->where('type', 'interest')->get();
        return $interests;
    }

    public function avgRatingInActivity($id) {
        $ratings = $this->activities()->where('activity_id', $id)->value('rating');
        $number = $this->activities()->where('activity_id', $id)->value('number_of_ratings');

        return $number === 0 ? 0 : (int)($ratings / $number);
    }

    public function activityRequests(){
        return $this->hasMany(ActivityRequest::class);
    }

    public function roleInActivity(Activity $activity) {
        $value = DB::table('activity_user')
            ->where('user_id', $this->id)
            ->where('activity_id', $activity->id)
            ->value('working_as');

        if (is_null($value)) {
            $value = DB::table('activity_user')
                ->where('user_id', $this->id)
                ->where('activity_id', $activity->id)
                ->value('type');
        }

        return title_case($value);
    }

    public function publicResumeLink() {
        return is_null($this->hash_code) ? null : url('/resume/' . $this->hash_code);
    }
}
