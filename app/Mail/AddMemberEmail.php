<?php

namespace ConnectInn\Mail;

use ConnectInn\Activity;
use ConnectInn\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddMemberEmail extends Mailable
{
    protected $activity;
    protected $receiver;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Activity $activity , User $receiver)
    {
        $this->receiver = $receiver;
        $this->activity = $activity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.memberAddedEmail' , [
            'activity' => $this->activity,
            'receiver' => $this->receiver
        ]);
    }
}
