<?php

namespace ConnectInn\Mail;

use ConnectInn\Activity;
use ConnectInn\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StoryAddedEmail extends Mailable
{
    protected $activity;
    protected $receiver;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param Activity $activity
     * @param User $receiver
     */
    public function __construct(Activity $activity , User $receiver)
    {
        $this->activity = $activity;
        $this->receiver = $receiver;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.storyAddedEmail' , [
            'activity' => $this->activity,
            'receiver' => $this->receiver
        ]);
    }
}
