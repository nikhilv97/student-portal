<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;



/**
 * ConnectInn\Milestone
 *
 * @property int $id
 * @property int $activity_id
 * @property string $type
 * @property string $status
 * @property string $title
 * @property string $description
 * @property int|null $assigned_user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \ConnectInn\Activity $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereAssignedUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Milestone whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Milestone extends Model
{
    public function activity(){
        return $this->belongsTo(Activity::class)->orderBy('created_at', 'desc');
    }
}
