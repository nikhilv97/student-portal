<?php
namespace ConnectInn\Services;
use ConnectInn\Activity;
use ConnectInn\Education;
use ConnectInn\Experience;
use ConnectInn\Http\Requests\DocumentAddRequest;
use ConnectInn\UserDocument;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;


class UserDocumentService{
    public function addDocument(DocumentAddRequest $request)
    {
        $user = Auth::user();

        $document = new UserDocument();

        $document->title = $request->getTitle();
        $document->url = $this->uploadDocument($request);

        if ($request->hasDescription()) {
            $document->description = $request->getDescription();
        }
        $document->user_id = $user->id;

        $document->save();


    }

    function uploadDocument(DocumentAddRequest $request)
    {
        $thumbnail = $request->getFile();
        $imageUrl = 'student-com/documents/' . uniqid() . time() . '.' . $thumbnail->getClientOriginalExtension();

        Storage::disk('s3')->put($imageUrl, file_get_contents($thumbnail->getRealPath()), 'public');

        $baseURL = 'https://s3-' . Config::get('filesystems.disks.s3.region') . '.amazonaws.com/' . Config::get('filesystems.disks.s3.bucket') . '/';
        $fullURL = $baseURL . $imageUrl;

        return $fullURL;

    }

    public function addActivityDocument(DocumentAddRequest $request , Activity $activity){
        $user = Auth::user();

        $document = new UserDocument();

        $document->title = $request->getTitle();
        $document->url = $this->uploadDocument($request);

        if ($request->hasDescription()) {
            $document->description = $request->getDescription();
        }
        $document->activity_id = $activity->id;
        $document->user_id = $user->id;

        $document->save();

    }

    public function addEducationDocument(DocumentAddRequest $request , Education $education){
        $user = Auth::user();

        $document = new UserDocument();

        $document->title = $request->getTitle();
        $document->url = $this->uploadDocument($request);

        if ($request->hasDescription()) {
            $document->description = $request->getDescription();
        }
        $document->education_id = $education->id;
        $document->user_id = $user->id;

        $document->save();

    }

    public function addExperienceDocument(DocumentAddRequest $request , Experience $experience){
        $user = Auth::user();

        $document = new UserDocument();

        $document->title = $request->getTitle();
        $document->url = $this->uploadDocument($request);

        if ($request->hasDescription()) {
            $document->description = $request->getDescription();
        }
        $document->experience_id = $experience->id;
        $document->user_id = $user->id;

        $document->save();

    }
}
