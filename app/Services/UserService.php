<?php


namespace ConnectInn\Services;

use Carbon\Carbon;
use ConnectInn\Activity;
use ConnectInn\Attribute;
use ConnectInn\Education;
use ConnectInn\Exceptions\UserNotFoundException;
use ConnectInn\Experience;
use ConnectInn\Http\Requests\AttributeAddRequest;
use ConnectInn\Http\Requests\EducationAddRequest;
use ConnectInn\Http\Requests\ExperienceAddRequest;
use ConnectInn\Http\Requests\UserUpdateRequest;
use ConnectInn\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 14/6/17
 * Time: 2:51 PM
 */
class UserService
{
    public function isLoggedIn(User $user)
    {
        return $user->id === Auth::id();
    }

    function getMemberRole(User $member, Activity $activity)
    {
        $role = $activity->users()->where('email', $member->email)->value('working_as');

        return is_null($role) ? 'Member' : $role;

    }

    function updateUser(UserUpdateRequest $request)
    {
        $user = Auth::user();
        $attr = $request->get('name');
        $value = $request->get('value');

        $method = camel_case($attr) . 'Validator';


        if (method_exists(ValidatorService::class, $method)) {
            $validator = Validator::make([
                $attr => $value
            ], ValidatorService::$method());
            if ($validator->fails()) {
                throw new ValidationException($validator->errors());
            }
        }

        $user->$attr = $value;

        $user->save();

        return $user;
    }

    function getUserForId($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    function attachUser($userId)
    {
        $user = Auth::user();
        $user->followers()->attach($userId);
    }

    function detachUser($userId)
    {
        $user = Auth::user();
        $user->followers()->detach($userId);
    }

    function getFollowers()
    {
        $user = Auth::user();
        return $user->followers;
    }

    function addEducation(EducationAddRequest $request)
    {
        $user = Auth::user();

        $education = new Education();
        $education->name = $request->getName();
        $education->degree = $request->getDegree();
        $education->grade = $request->getGrade();
        $education->grade_type = $request->getGradeType();
        $education->from = $request->getFrom();
        $education->location = $request->getLocation();

        $education->official_body = $request->getOfficialBody();
        $education->user_id = $user->id;

        if ($request->hasTo()) {
            $education->to = $request->getTo();
        }

        $education->save();

        return $education;

    }

    function addExperience(ExperienceAddRequest $request)
    {
        $user = Auth::user();
        $experience = new Experience();

        $experience->organisation_name = $request->getName();
        $experience->description = $request->getDescription();
        $experience->designation = $request->getDesignation();
        $experience->from = $request->getFrom();
        $experience->location = $request->getLocation();
        $experience->user_id = $user->id;

        if ($request->hasTo()) {
            $experience->to = $request->getTo();
        }

        $experience->save();

        return $experience;
    }

    function addAttribute(AttributeAddRequest $request)
    {
        $user = Auth::user();
        $attribute = new Attribute();

        $attribute->type = $request->getType();
        $attribute->value = $request->getValue();
        $attribute->user_id = $user->id;

        $attribute->save();

        return $attribute;
    }


    public static function resumeJson()
    {
        $user = Auth::user();
        $data = [
            'resume' => [
                'basics' => [
                    'name' => $user->name,
                    'picture' => $user->profile_pic_url,
                    'email' => $user->email,
                    'phone' => $user->phone_number ? $user->phone_number : '',
                    'summary' => $user->about ? $user->about : '',
                    'location' => [
                        'address' => '',
                        'postalCode' => 'CA 94115',
                        'city' => 'Ghaziabad',
                        'country' => 'India',
                        'region' => 'California'
                    ]
                ],
                'references' => []
            ]
        ];

        $volunteers = Activity::join('activity_user', 'activities.id', '=', 'activity_user.activity_id')
            ->where('activity_user.user_id', '=', Auth::id())
            ->where('activities.type', '=', 'volunteer')
            ->select('activities.*')
            ->get();

        foreach ($volunteers as $key => $volunteer) {
            $data['resume']['volunteer'][$key] = [
                'organization' => title_case($volunteer->title),
                'summary' => $volunteer->desc,
                'startDate' => Carbon::parse($volunteer->start)->toDateString(),
                'endDate' => is_null($volunteer->end) ? '' : Carbon::parse($volunteer->end)->toDateString()
            ];
        }

        $awards = Activity::join('activity_user', 'activities.id', '=', 'activity_user.activity_id')
            ->where('activity_user.user_id', '=', Auth::id())
            ->where('activities.type', '=', 'certification')
            ->select('activities.*')
            ->get();

        foreach ($awards as $key => $award) {
            $data['resume']['awards'][$key] = [
                'title' => title_case($award->title),
                'summary' => $award->desc,
                'date' => Carbon::parse($award->end)->toDateString(),
                'awarder' => ''
            ];
        }

        $publications = Activity::join('activity_user', 'activities.id', '=', 'activity_user.activity_id')
            ->where('activity_user.user_id', '=', Auth::id())
            ->where('activities.type', '=', 'publication')
            ->select('activities.*')
            ->get();

        foreach ($publications as $key => $publication) {
            $data['resume']['publications'][$key] = [
                'name' => title_case($publication->title),
                'summary' => $publication->desc,
                'releaseDate' => Carbon::parse($publication->start)->toDateString(),
                'awarder' => ''
            ];
        }

        foreach ($user->experiences as $key => $experience) {
            $data['resume']['work'][$key] = [
                'company' => title_case($experience->organisation_name),
                'position' => title_case($experience->designation),
                'startDate' => Carbon::create($experience->from, 1, 1, 0, 0, 0)->toDateString(),
                'endDate' => is_null($experience->to) ? '' : Carbon::create($experience->to, 1, 1, 0, 0, 0)->toDateString(),
                'summary' => $experience->description,
                'highlights' => []
            ];
        }

        $works = Activity::join('activity_user', 'activities.id', '=', 'activity_user.activity_id')
            ->where('activity_user.user_id', '=', Auth::id())
            ->where(function ($query) {
                $query->where('activities.type', '=', 'project')
                    ->orWhere('activities.type', '=', 'seminar')
                    ->orWhere('activities.type', '=', 'workshop')
                    ->orWhere('activities.type', '=', 'guest_lecture')
                    ->orWhere('activities.type', '=', 'other');
            })
            ->select('activities.*')
            ->get();

        foreach ($works as $key => $work) {
            $i = $user->experiences->count() + $key;
            $data['resume']['work'][$i] = [
                'company' => title_case($work->title),
                'position' => '',
                'startDate' => Carbon::parse($work->start)->toDateString(),
                'endDate' => is_null($work->end) ? '' : Carbon::parse($work->end)->toDateString(),
                'summary' => $work->desc,
                'highlights' => []
            ];
        }

        foreach ($user->educations as $key => $education) {
            $data['resume']['education'][$key] = [
                'institution' => title_case($education->name) . ', ' . title_case($education->location),
                'area' => '',
                'studyType' => title_case($education->degree),
                'startDate' => Carbon::create($education->from, 1, 1, 0, 0, 0)->toDateString(),
                'endDate' => is_null($education->to) ? '' : Carbon::create($education->to, 1, 1, 0, 0, 0)->toDateString(),
                'gpa' => (string)$education->grade,
                'courses' => []
            ];
        }

        foreach ($user->skills() as $key => $skill) {
            $data['resume']['skills'][$key] = [
                'name' => title_case($skill->value),
                'level' => '',
                'keywords' => []
            ];
        }

        foreach ($user->interests() as $key => $interest) {
            $data['resume']['interests'][$key] = [
                'name' => title_case($interest->value),
                'level' => '',
                'keywords' => []
            ];
        }

        if ($user->languages) {
            $languages = \GuzzleHttp\json_decode($user->languages, true);
            foreach ($languages as $key => $language) {
                $data['resume']['languages'][$key] = [
                    'language' => title_case($language['language']),
                    'fluency' => title_case($language['fluency'])
                ];
            }
        }

        return $data;
    }


}