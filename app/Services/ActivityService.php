<?php

namespace ConnectInn\Services;

use ConnectInn\Activity;
use ConnectInn\ActivityRequest;
use ConnectInn\Exceptions\ActivityNotFoundException;
use ConnectInn\Http\Requests\ActivityAddMemberRequest;
use ConnectInn\Http\Requests\ActivityCreateRequest;
use ConnectInn\Http\Requests\ActivityUpdateRequest;
use ConnectInn\Http\Requests\UpadteRatingRequest;
use ConnectInn\Tag;
use ConnectInn\User;
use DB;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 14/6/17
 * Time: 3:47 PM
 */
class ActivityService
{
    static function find($id)
    {
        $activity = Activity::find($id);

        if (!$activity) {
            throw new ActivityNotFoundException();
        }

        return $activity;
    }

    function create(ActivityCreateRequest $request)
    {
        $title = $request->get('title');
        $description = $request->get('desc');
        $type = $request->get('type');
        $start = $request->get('start');
        $end = $request->get('end');

        $user = Auth::user();

        $activity = new Activity();
        $activity->title = $title;
        $activity->desc = $description;
        $activity->type = $type;
        $activity->start = $start;
        $activity->end = $end;
        $activity->link = $request->get('link');
        $activity->external_link = $request->get('external_link');
        $user->activities()->save($activity);

        foreach ($request->get('tags') as $item) {
            $tag = Tag::where('slug', $item)->first();

            $activity->tags()->attach($tag);
        }

        return $activity;
    }

    function delete($activityid)
    {
        $activity = Activity::find($activityid);

        if (is_null($activity)) {
            throw new ActivityNotFoundException();
        }

        $activity->delete();

    }

    function update(ActivityUpdateRequest $request, $activity)
    {
        $activity = Activity::find($activity);
        $attr = $request->get('name');
        $value = $request->get('value');
        $activity->$attr = $value;

        $activity->save();
    }

    function addMember(ActivityAddMemberRequest $request, $id)
    {
        $activity = Activity::find($id);

        $member = User::where('email', $request->getEmail())->first();

        if (!($activity->users->contains($member->id))) {
            $data = [
                'type' => 'member'
            ];

            if ($request->hasWorkingAs()) {
                $data['working_as'] = $request->getWorkingAs();
            }

            $activity->users()->attach($member, $data);

        }
    }

    function detachMember($id, $user_id)
    {
        $activity = Activity::find($id);
        $activity->users()->detach($user_id);

    }

    function updateRating(UpadteRatingRequest $request, Activity $activity, User $user)
    {
        DB::table('activity_user')->where('user_id', $user->id)
            ->where('activity_id', $activity->id)
            ->update([
                'rating' => $request->getRating(),
                'number_of_ratings' => $activity->users()->where('user_id', $user->id)->value('number_of_ratings') + 1
            ]);

        $numberOfRatings = $activity->users()->where('user_id', $user->id)->sum('number_of_ratings');
        $totalRatings = $activity->users()->where('user_id', $user->id)->sum('rating');

        $user->avg_rating = $numberOfRatings === 0 ? 0 : $totalRatings / $numberOfRatings;

        $user->save();
    }

    function sendConnectRequest(Activity $activity)
    {
        $user = Auth::user();

        $activityRequest = new ActivityRequest();

        $activityRequest->user_id = $user->id;
        $activityRequest->activity_id = $activity->id;

        $activityRequest->save();
    }

    function acceptRequest($requestId)
    {
        $activityRequest = ActivityRequest::find($requestId);
        $activity = $activityRequest->activity;
        $user = $activityRequest->user;

        $activity->users()->attach($user, ['type' => 'member']);
        $activityRequest->delete();
    }

    function rejectRequest($requestId)
    {
        $activityRequest = ActivityRequest::find($requestId);
        $activityRequest->is_rejected = false;

        $activityRequest->save();
    }

}