<?php
/**
 * Created by PhpStorm.
 * User: nik
 * Date: 6/4/18
 * Time: 4:19 PM
 */

namespace ConnectInn\Services;


use ConnectInn\Community_Forums;
use ConnectInn\Exceptions\ForumNotFoundException;
use ConnectInn\Http\Requests\CommunityForumAddRequest;
use ConnectInn\Http\Requests\CommunityForumAddUserRequest;
use ConnectInn\Http\Requests\CommunityForumRemoveUserRequest;
use ConnectInn\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommunityForumService
{
    static function find($id)
    {
        $forum = Community_Forums::find($id);

        if (!$forum) {
            throw new ForumNotFoundException();
        }

        return $forum;
    }

    function create(CommunityForumAddRequest $request){
        $title = $request->get('title');
        $desc = $request->get('description');
        $isPublic = $request->get('isPublic');

        $owner = Auth::user();

        $forum = new Community_Forums();
        $forum->owner_id = $owner->id;
        $forum->title = $title;
        $forum->description = $desc;
        $forum->isPublic = $isPublic;

        $forum->save();

        return $forum;
    }

    function delete(Request $request,$id){
        $forum = CommunityForumService::find($id);
        $forum->delete();
    }

     function addUsers(CommunityForumAddUserRequest $request,$id){

        $emails = $request->get('email');
          foreach ( $emails as $email){
              $user = User::where('email', $email)->first();

              DB::table('community_forum_users')->insertGetId(
                  ['user_id' => $user->id,'forum_id'=>$id]
              );

          }

     }

     function removeUser(CommunityForumRemoveUserRequest $request,$id){
               $email = $request->get('email');
               $user = User::where('email', $email)->first();
               DB::table('community_forum_users')
                   ->where('user_id','=',$user->id)
                   ->where('forum_id','=',$id)
                   ->delete();
     }

}