<?php
/**
 * Created by PhpStorm.
 * User: kushagra
 * Date: 11/08/17
 * Time: 9:34 AM
 */

namespace ConnectInn\Services;


class ValidatorService
{
    public static function languageValidator() {
        return [
            'language' => 'required|string',
            'fluency' => 'required|in:elementary,limited,professional,full_professional,bilingual'
        ];
    }

    public static function phoneNumberValidator() {
        return [
            'phone_number' => 'phone_number'
        ];
    }

    public static function emailValidator() {
        return [
            'email' => 'email|unique:users,email'
        ];
    }

    public function dobValidator() {
        return [
            'dob' => 'date'
        ];
    }
}