<?php
/**
 * Created by PhpStorm.
 * User: kushagra
 * Date: 14/08/17
 * Time: 11:20 PM
 */

namespace ConnectInn\Services;


use Auth;
use ConnectInn\Article;
use ConnectInn\Exceptions\ArticleNotFoundException;
use ConnectInn\Http\Requests\ArticleCreateRequest;

class ArticleService
{
    /**
     * @param $id
     * @return mixed|Article
     */
    public static function find($id) {
        $article = Article::find($id);

        if (!$article) {
            throw new ArticleNotFoundException();
        }

        return $article;
    }

    public function save(ArticleCreateRequest $request) {
        $article = new Article();

        $article->text = $request->getText();
        $article->user_id = Auth::id();

        $article->save();
    }

    public function update() {

    }

}