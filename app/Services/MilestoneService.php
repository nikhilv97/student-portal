<?php
/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 26/6/17
 * Time: 6:12 PM
 */

namespace ConnectInn\Services;


use ConnectInn\Activity;
use ConnectInn\Http\Requests\ActivityAddStoryRequest;
use ConnectInn\Http\Requests\ActivityAddTaskRequest;
use ConnectInn\Milestone;
use ConnectInn\User;

class MilestoneService
{
    function saveStory(ActivityAddStoryRequest $request, Activity $activity)
    {
        $milestone = new Milestone();


        $milestone->title = $request->getTitle();
        $milestone->description = $request->getDescription();
        $milestone->type = $request->getType();
        $milestone->status = $request->getStatus();

        $activity->milestones()->save($milestone);

        return $milestone;

    }

    function saveTask(ActivityAddTaskRequest $request, Activity $activity)
    {
        $milestone = new Milestone();
        $user = User::where('email', $request->getEmail() )->first();

        $milestone->title = $request->getTitle();
        $milestone->description = $request->getDescription();
        $milestone->type = $request->getType();
        $milestone->status = $request->getStatus();
        $milestone->assigned_user_id = $user->id;


        $activity->milestones()->save($milestone);

        return $milestone;

    }

    function update(Milestone $milestone, $attribute, $value){
        $milestone->$attribute = $value;

        $milestone->save();
    }
}