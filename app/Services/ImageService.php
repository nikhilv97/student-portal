<?php
/**
 * Created by PhpStorm.
 * User: pragati
 * Date: 15/6/17
 * Time: 12:15 PM
 */

namespace ConnectInn\Services;


use ConnectInn\Http\Requests\ImageUploadRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    function uploadImage(ImageUploadRequest $request)
    {
        $thumbnail = $request->getImage();
        $imageUrl = 'student-com/profile-pics/'.uniqid() . time() . '.' . $thumbnail->getClientOriginalExtension();

        Storage::disk('s3')->put($imageUrl, file_get_contents($thumbnail->getRealPath()), 'public');

        $baseURL = 'https://s3-' . Config::get('filesystems.disks.s3.region') . '.amazonaws.com/' . Config::get('filesystems.disks.s3.bucket') . '/';
        $fullURL = $baseURL . $imageUrl;

        return $fullURL;
    }



}