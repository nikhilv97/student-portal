<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\Like
 *
 * @property int $id
 * @property int $user_id
 * @property int $article_id
 * @property int $like
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \ConnectInn\Article $article
 * @property-read \ConnectInn\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Like whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Like whereLike($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Like whereUserId($value)
 * @mixin \Eloquent
 */
class Like extends Model
{
    public function user(){

        return $this->belongsTo(User::class);
    }


    public function article(){
        return $this->belongsTo(Article::class);
    }
}
