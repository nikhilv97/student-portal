<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\Article
 *
 * @property int $id
 * @property string $text
 * @property int $user_id
 * @property string|null $image_url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\Like[] $likes
 * @property-read \ConnectInn\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Article whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Article whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Article whereUserId($value)
 * @mixin \Eloquent
 */
class Article extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes(){
        return $this->hasmany(Like::class);
    }
}
