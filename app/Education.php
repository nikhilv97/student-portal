<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\Education
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\UserDocument[] $documents
 * @property-read \ConnectInn\User $user
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $degree
 * @property string $grade_type
 * @property int $grade
 * @property int $from
 * @property int|null $to
 * @property string $official_body
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereGrade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereGradeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereOfficialBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereUpdatedAt($value)
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereUserId($value)
 * @property string|null $location
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Education whereLocation($value)
 */
class Education extends Model
{
    protected $table = 'educations';

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function  documents(){
        return $this->hasMany(UserDocument::class);
    }
}
