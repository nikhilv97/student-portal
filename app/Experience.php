<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\Experience
 *
 * @property int $id
 * @property string $organisation_name
 * @property string $designation
 * @property string|null $description
 * @property int $from
 * @property int|null $to
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\ConnectInn\UserDocument[] $documents
 * @property-read \ConnectInn\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereDesignation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereOrganisationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereUserId($value)
 * @property string|null $location
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Experience whereLocation($value)
 */
class Experience extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function  documents(){
        return $this->hasMany(UserDocument::class);
    }
}
