<?php

namespace ConnectInn\Jobs;

use ConnectInn\Activity;
use ConnectInn\Mail\StoryAddedEmail;
use ConnectInn\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendStoryAddedEmailJob implements ShouldQueue
{
    protected  $activity;
    protected  $receiver;
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( Activity $activity , User $receiver)
    {
        $this->activity = $activity;
        $this->receiver = $receiver;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->receiver)->send(new StoryAddedEmail($this->activity , $this->receiver));

    }
}
