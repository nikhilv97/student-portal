<?php

namespace ConnectInn\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('phone_number', function($attribute, $value, $parameters, $validator) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            try {
                $contactNumber = $phoneUtil->parse($value, null);
                $isValid = $phoneUtil->isValidNumber($contactNumber);
            } catch (NumberParseException $e) {
                return false;
            }
            return $isValid;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
