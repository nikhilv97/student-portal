<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\Attribute
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $value
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \ConnectInn\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Attribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Attribute whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Attribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Attribute whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Attribute whereValue($value)
 * @mixin \Eloquent
 */
class Attribute extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
}
