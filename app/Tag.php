<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\Tag
 *
 * @property int $id
 * @property string $value
 * @property string $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \ConnectInn\Activity $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Tag whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Tag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\Tag whereValue($value)
 * @mixin \Eloquent
 */
class Tag extends Model
{
    public function activity() {
        return $this->belongsTo(Activity::class);
    }
}
