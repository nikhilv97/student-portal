<?php

namespace ConnectInn;

use Illuminate\Database\Eloquent\Model;

/**
 * ConnectInn\ActivityRequest
 *
 * @property int $id
 * @property int $user_id
 * @property int $activity_id
 * @property int|null $is_rejected
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \ConnectInn\Activity $activity
 * @property-read \ConnectInn\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\ActivityRequest whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\ActivityRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\ActivityRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\ActivityRequest whereIsRejected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\ActivityRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ConnectInn\ActivityRequest whereUserId($value)
 * @mixin \Eloquent
 */
class ActivityRequest extends Model
{
    public function user(){
        return  $this->belongsTo(User::class);
    }

    public function activity(){
        return $this->belongsTo(Activity::class);
    }
}
