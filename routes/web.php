<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','PagesController@home');

Route::post('/write-us', 'HomeController@writeUs');

Route::any('resume/{hash}', function ($hash) {
    $resumeHtml = \ConnectInn\User::where('hash_code', $hash)->value('resume_html');

    return $resumeHtml;
});




Auth::routes();



// Routes for logged in user
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@showHomePage');
    Route::get('myprofile','UserController@index');

    Route::get('test', function () {
        $user = Auth::user();
        $requests = $user->activityRequests->load('user');
        dd($requests);
    });

    Route::get('user/{user}/timeline','UserController@showProfile');
    Route::get('user/search','UserController@search');
    Route::patch('user/updated','UserController@update');
    Route::get('/me/resume', 'UserController@downloadResume');
    Route::post('me/educations', 'UserController@addEducation');
    Route::post('/me/experiences', 'UserController@addExperience');
    Route::post('me/languages', 'UserController@addLanguage');
    Route::post('me/documents', 'UserController@addDocument');
    Route::post('me/attributes', 'UserController@addattribute');
    Route::delete('attributes/{attribute}', 'UserController@deleteAttribute');
    Route::delete('experience/{experience}', 'UserController@deleteExperience');
    Route::delete('education/{education}', 'UserController@deleteEducation');
    Route::delete('documents/{document}', 'UserController@deleteDocument');


    Route::delete('educations/{education}', 'EducationController@destroy');
    Route::delete('experiences/{experience}', 'ExperienceController@destroy');

    Route::get('search', 'SearchController@search');

    Route::post('/activity', 'ActivityController@createNewActivity');
    Route::get('/activities', 'ActivityController@index');
    Route::get('/activities/search', 'ActivityController@search');
    Route::delete('activity/{activity_id}', 'ActivityController@delete');
    Route::get('activity/{id}','ActivityController@show');
    Route::patch('activity/{activity}', 'ActivityController@update');
    Route::post('/activity/{id}/users/attach', 'ActivityController@addMember');
    Route::get('/activity/{id}/users/{user_id}/detach', 'ActivityController@removeMember');
    Route::post('/activity/{activity}/users/{user}/rating', 'ActivityController@updateRating');
    Route::post('/activity/{activity}/documents', 'ActivityController@addActivityDocument');
    Route::delete('documents/{documents}' , 'ActivityController@deleteDocument');
    Route::post('activity/{activity}/request' , 'ActivityController@sendConnectToActivityRequest');
    Route::post('activity-request/{activityRequest}/accept' , 'ActivityController@acceptRequest');
    Route::post('activity-request/{activityRequest}/reject' , 'ActivityController@rejectRequest');


    Route::post('/education/{education}/documents', 'UserController@addEducationDocument');
    Route::post('/experience/{experience}/documents', 'UserController@addExperienceDocument');

    Route::post('/activity/{activity}/milestones/story', 'ActivityMilestoneController@saveStory');
    Route::post('/activity/{activity}/milestones/task', 'ActivityMilestoneController@saveTask');
    Route::patch('/milestones/{milestone}', 'MilestoneController@update');

    Route::post('/profile-pic','UserController@profilePic');

    Route::get('/followers/{user}/attach','UserController@addFollower');
    Route::get('/followers/{user}/detach','UserController@removeFollower');
    Route::get('/followers','UserController@fetchFollowers');

    //Articles Related
    Route::resource('articles', 'ArticleController');

    //Like
    Route::post('article/like/{activity}','ArticleController@like');
    Route::post('article/unlike/{activity}','ArticleController@unlike');

    //Community_Forum
    Route::post('forums','Community_ForumController@createNewForum');
    Route::delete('forums/{forum_id}','Community_ForumController@deleteForum');
    Route::get('forums','Community_ForumController@index');
    Route::get('forums/{forum_id}/live','Community_ForumController@liveForum');
    Route::post('forums/{forum_id}/add-users','Community_ForumController@addUsers');
    Route::delete('forums/{forum_id}/remove-users','Community_ForumController@removeUser');




});

