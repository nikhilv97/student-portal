<?php

use ConnectInn\experience;
use ConnectInn\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ExperienceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::all()->pluck('id')->toArray();
        $designations = ['assisstant', 'developer', 'analyst', 'manager'];

        for ($i = 0; $i < 100; $i++)
        {
            $gradeType = $faker->randomElement(['cgpa', 'percentage']);;

            $experience = new Experience();
            $experience->organisation_name = $faker->company;
            $experience->designation = $faker->randomElement($designations);
            $experience->description = $faker->text();
            $experience->user_id = $faker->randomElement($users);

            $experience->from = $faker->year;
            $experience->to = $faker->year;
            $experience->location = $faker->city;

            $experience->save();
        }
    }
}
