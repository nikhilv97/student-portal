<?php

use ConnectInn\Education;
use ConnectInn\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class EducationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::all()->pluck('id')->toArray();
        $schools = ['L.P.S', 'St. Don Bosco', 'C.M.S', 'St, Francis', 'La Martinere'];
        $degrees = ['high_school', 'intermmediate'];

        for ($i = 0; $i < 100; $i++)
        {
            $gradeType = $faker->randomElement(['cgpa', 'percentage']);;

            $education = new Education();
            $education->name = $faker->randomElement($schools);
            $education->degree = $faker->randomElement($degrees);
            $education->grade_type = $gradeType;

            if ($gradeType === 'cgpa') {
                $education->grade = rand(0, 10);
            } else {
                $education->grade = rand(0, 100);
            }

            $education->from = $faker->year;
            $education->to = $faker->year;
            $education->official_body = 'ICSE';
            $education->user_id = $faker->randomElement($users);
            $education->location = $faker->city;

            $education->save();
        }
    }
}
