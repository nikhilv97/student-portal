<?php

use ConnectInn\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $user = new User();
        $user->name = "Pragati Shekhar";
        $user->password = bcrypt('poko');
        $user->email = 'shekharpragati143@gmail.com';

        $user->save();

        for ($i = 0; $i < 100; $i++)
        {
            $user = new User();
            $user->name = $faker->name;
            $user->password = bcrypt('poko');
            $user->email = $faker->email;
            $user->avg_rating = random_int(0, 5);
            $user->profile_pic_url = $faker->imageUrl();
            $user->about = $faker->realText(200);

            $user->save();
        }

    }
}
