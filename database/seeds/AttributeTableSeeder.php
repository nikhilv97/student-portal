<?php

use ConnectInn\Attribute;
use ConnectInn\User;

use Faker\Factory;
use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::all()->pluck('id')->toArray();
        $type = ['skill', 'interest'];
        $value = ['php', 'java', 'javascript', 'css', 'management', 'marketing', 'python', 'AI', 'machine learning'];


        for ($i = 0; $i < 100; $i++)
        {

            $attribute = new Attribute();
            $attribute->type = $faker->randomElement($type);
            $attribute->value = $faker->randomElement($value);
            $attribute->user_id = $faker->randomElement($users);


            $attribute->save();
        }
    }
}
