<?php
;
use ConnectInn\Activity;
use ConnectInn\Attribute;
use ConnectInn\Education;
use ConnectInn\Experience;
use ConnectInn\Milestone;
use ConnectInn\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        User::truncate();
        Activity::truncate();
        Milestone::truncate();
        DB::table('activity_user')->truncate();
        DB::table('followers')->truncate();
        Education::truncate();
        Experience::truncate();
        Attribute::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ActivityTableSeeder::class);
        $this->call(ActivityUserTableSeeder::class);
        $this->call(MilestoneTableSeeder::class);
        $this->call(EducationTableSeeder::class);
        $this->call(ExperienceTableSeeder::class);
        $this->call(FollowerTableSeeder::class);
        $this->call(AttributeTableSeeder::class);
        $this->call(TagTableSeeder::class);

        Model::reguard();
    }
}
