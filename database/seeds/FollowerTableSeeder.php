<?php

use Carbon\Carbon;
use ConnectInn\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class FollowerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::all()->pluck('id')->toArray();

        for ($i = 0; $i < 100; $i++)
        {
            DB::table('followers')
                ->insert([
                    'user_id' => $faker->randomElement($users),
                    'follower_id' => $faker->randomElement($users),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]);
        }
    }
}
