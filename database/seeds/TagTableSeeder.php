<?php

use ConnectInn\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Tag::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Model::unguard();

        $tags = [
            'cse',
            'ec',
            'me',
            'en',
            'civil',
            'ce',
            'it',
            'engineering',
            'coding',
            'php',
            'java', 'jsp', 'seminar', 'website', 'javascript', 'angular', 'digital marketing'
        ];

        foreach ($tags as $tag) {
            $t = new Tag();
            $t->value = $tag;
            $t->slug = str_slug($tag);

            $t->save();
        }

        Model::reguard();


    }
}
