<?php

use ConnectInn\Activity;
use ConnectInn\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = User::all()->pluck('id')->toArray();
        $activities = Activity::all()->pluck('id')->toArray();
        foreach ($activities as $activity) {
            DB::table('activity_user')->insert(
                [
                    'user_id' => $faker->randomElement($users),
                    'activity_id' => $activity
                ]
            );

        }

    }
}
