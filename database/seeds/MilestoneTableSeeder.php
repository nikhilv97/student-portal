<?php

use ConnectInn\Activity;
use ConnectInn\Milestone;
use ConnectInn\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class MilestoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $activity = Activity::all()->pluck('id')->toArray();
        $users = User::all()->pluck('id')->toArray();
        $type = ['story', 'task'];
        $status = ['created', 'started', 'finished','accepted','rejected'];


        for ($i = 0; $i < 100; $i++) {
            $t = $faker->randomElement($type);
            $array = [
                'activity_id' => $faker->randomElement($activity),
                'type' => $t,
                'status' => $faker->randomElement($status),
                'title' => $faker->realText(50),
                'description' => $faker->realText(200)
            ];
            if ($t == 'task'){
                $array['assigned_user_id'] = $faker->randomElement($users);
            }
                Milestone::create($array);


        }
    }
}
