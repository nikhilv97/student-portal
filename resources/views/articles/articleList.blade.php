@php
    if(!isset($articles)){
    $articles = \Illuminate\Support\Facades\Auth::user()->articles;
    }
@endphp


<ul id="articleList" class="list-group">
    @each('articles.articleListItem', $articles, 'article', 'placeholders.noActivities')
</ul>