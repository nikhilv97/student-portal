@inject('userService', 'ConnectInn\Services\UserService')

@php
    $user = $article->user;

@endphp

<div class="panel panel-default activitybox">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <img src="{{$user->profile_pic_url}}" height="50" width="50" alt="pic"
                             class="img-circle">
                        <a class="username" href="/user/{{ $user->id}}/timeline">{{ $user->name}} </a>
                        <div>
                            <hr style="border: 1px solid darkgray">
                        </div>
                    </div>

                    @if($user->id==(\Illuminate\Support\Facades\Auth::user()->id))
                        <div class="col-md-4">
                            <div class="row pull-right">
                                <div class="col-md-2">

                                    <button class="btn btn-link"
                                            style="float: right; color: midnightblue"
                                    ><i class="fa fa-pencil-square-o"></i></button>

                                </div>
                                <div class="col-md-2">

                                    <form action="{{url("/articles/$article->id")}}" method="POST">
                                        {{method_field('DELETE')}}
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-link" style="float: right; color: maroon"
                                        ><i class="fa fa-times"></i></button>

                                    </form>

                                </div>

                            </div>


                        </div>
                    @endif

                </div>
                <div class=" col-md-12 row">
                    @php
                    $text = $article->text;
                        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
                    if(preg_match($reg_exUrl, $text, $url)) {
                           // make the urls hyper links
                           $text = preg_replace($reg_exUrl, "<a target='_blank' href='{$url[0]}''>{$url[0]}</a> ", $text);
                    }
                    @endphp
                    <div style="font-size: 15px;color: #122b40;padding-left: 10px;"><p>{!! $text !!}</p></div>
                </div>

                <div class=" col-md-12 row">
                    <hr style="border: 0.5px solid lightgray">
                </div>

                <div  class="row">
                    <div class="col-md-12">
                        <div id="interestdiv" onclick="like({{$article->id}})">
                        <div class="btn btn-danger">
                            <i class="fa fa-hand-o-up" style="font-size: 1em;margin-left: 2px">&nbsp;Show Interest</i>
                        </div>
                            @foreach($article->likes as $like)
                                @if($like->user_id==Auth::id())
                                    &nbsp; You and
                                    @break(0);
                                @endif
                            @endforeach
                           &nbsp;{{$article->likes->count()}} Other Have Shown Interest

                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>
</div>

<script>
    function like(like) {

        $.ajax('article/like/' + like, {
            method: 'POST',
            data: {
                _token: '{{csrf_token()}}'
            },
            success: function (response, status, jqXHR) {
                console.log('success on like' + status + response+ '' + jqXHR);
                if (response=='    likesaved') {
                    setTimeout(
                        function()
                        {
                            location.reload();
                        }, 0001);
                }
            },
            error: function (a, b, c) {
                console.log('error');

            }
        })
    }
</script>
