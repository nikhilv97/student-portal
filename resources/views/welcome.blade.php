<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A portal to connect with peers and explore.">
    <meta name="author" content="Pragati Shekhar & Nikhil Verma">
    <meta name="keywords" content="students,communicate,skills,peers,community,resume builder,resume,profile">
    <meta name="robots" content="index,follow">

    {{--Facebook Open Graph--}}
    <meta property="og:title" content="ConnectInn">
    <meta property="og:type" content="article">
    <meta property="og:description" content="Connecting Young Professionals For a Productive Future">
    <meta property="og:image" content="http://www.connectinn.tk/bg_all.jpg">

    {{--Twitter Cards--}}
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="ConnectInn" />
    <meta name="twitter:description" content="Connecting Young Professionals For a Productive Future" />
    <meta name="twitter:image" content="http://www.connectinn.tk/bg_all.jpg" />

    <title>ConnectInn</title>





    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="/css/freelancer.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
          rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
          type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="favicon.png">


</head>

<body id="page-top" class="">
{{--<div id="skipnav"><a href="#page-top">Skip to main content</a></div>--}}

<!-- Navigation -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top" style="background: #fff;padding: 10px 10px; border-radius: 2px;box-shadow: 3px 4px 6px rgba(70,70,0,0.70);">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand"
               style="color: #d43f3a;	font-family: 'Lobster', Georgia, Times, serif;font-size: 40px;line-height:22px;"
               href="#page-top">ConnectInn</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                    <a href="#login" style="color: #b93838">Login/SignUp</a>
                </li>
                <li class="page-scroll">
                    <a href="#about" style="color: #b93838">About</a>
                </li>
                <li class="page-scroll">
                    <a href="#contact" style="color: #b93838">Write Us</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->

<header>
    <div class="container" id="maincontent">

        <div class="row">
            <div class="col-lg-12">
               {{-- <img class="img-rounded img-responsive" src="/public/profile.png" alt="" height="200" width="200">--}}
                <div class="intro-text">
                    <h1 class="name" style="color: #b93838">Connect Inn</h1>
                    <br />
                    {{--<hr class="star-primary">--}}
                    <span class="skills" style="color: #b93838">Connecting Young Professionals For a Productive Future</span>
                </div>
            </div>

        </div>

    </div>
</header>

<!-- Portfolio Grid Section -->
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 style="color: #b93838">Login / SignUp</h2>
                <hr class="star-primary" >
                <div class="col-lg-12">
                    <div class="panel panel-default ">
                        <div class="panel-body">
                            <div>
                                <!-- Nav tabs -->
                                <div class="col-md-12">
                                    <ul class="nav nav-pills col-md-12" role="">
                                    <li style="margin-left: 0px;" class="col-md-6 active" role="presentation" class="active col-md-6 col-xs-6"><a href="#logIn"
                                                                                                aria-controls="story"
                                                                                                role="tab"
                                                                                                data-toggle="tab">
                                            LogIn

                                        </a></li>
                                        <li style="margin-left: 0px" class="col-md-6" role="presentation" class="col-md-6 col-xs-6"><a href="#signUp"
                                                                                         aria-controls="task"
                                                                                         role="tab" data-toggle="tab">
                                            SignUp

                                        </a>


                                    </li>
                                </ul>
                                </div>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="logIn">
                                        @include('auth.login')
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="signUp">
                                        @include('auth.register')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About Section -->
<section class="success" id="about" style="background: #b93838">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>About</h2>
                <hr class="star-light">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-lg-offset-2">
                <p>ConnectInn is a free portal theme created on Laravel. The other languages included in this project are HTML , CSS, AJAX ,JavaScript , JQuery. This portal helps you to develop, find or showcase your interests.  </p>
            </div>
            <div class="col-lg-4">
                <p>Whether you're a student looking to showcase your work, a professional looking to attract clients, or
                    a graphic artist looking to share your projects, this portal is the perfect starting point!</p>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 style="color: #b93838">Write to us</h2>
                <hr class="star-primary">
            </div>
        </div>
        <div class="row" >
            <div class="col-lg-8 col-lg-offset-2" id="contactFormDiv">
                <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                <form name="sentMessage" id="form" action="/write-us" method="post">
                    {{csrf_field()}}
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" placeholder="Name" id="name" required
                                   data-validation-required-message="Please enter your name.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" placeholder="Email Address" id="email" required
                                   data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="message">Message</label>
                            <textarea rows="5" class="form-control" placeholder="Message Query or Feedback" id="message" required
                                      data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button id="post" type="submit" onclick="submitStory()" class="btn btn-success btn-lg" style="background: #b93838; border-color: #b93838">Send
                                <i class="fa fa-spinner fa-spin pull-right" id="sendLoaderDiv" style="visibility: hidden" aria-hidden="true"></i>
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-4">
                <h3>Location</h3>
                <p>ABES Engineering College
                <br>NH 24, Ghaziabad, U.P, India</p>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Around the Web</h3>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/pragati.shekhar" target="_blank" class="btn-social btn-outline"><span class="sr-only">Facebook</span><i
                                        class="fa fa-fw fa-facebook"></i></a>
                        </li>

                        <li>
                            <a href="https://www.linkedin.com/in/pragati-shekhar-7b2a46121/" target="_blank" class="btn-social btn-outline"><span class="sr-only">Linked In</span><i
                                        class="fa fa-fw fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="https://bitbucket.org/pragati123/" target="_blank" class="btn-social btn-outline"><span class="sr-only">Bitbucket</span><i
                                        class="fa fa-fw fa-bitbucket"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3>About ConnectInn</h3>
                    <p>ConnectInn is a free portal to connect with peers, created by <strong>Pragati Shekhar, Nikhil
                            Verma,Kushagra Saxena</strong>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Copyright &copy; ConnectInn 2018
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- jQuery -->
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

<!-- Bootstrap Core JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<!-- Plugin JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="js/freelancer.min.js"></script>

<script>

    $(document).ready(function (e) {
       location.href = "#page-top";
         console.log();
    });

    function submitStory() {
        $('#post').button('loading');

        $('#form').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    $('#post').button('reset');
                    alert('Your response has been recorded. Thank You!!');

                    console.log(status);
                },

                failure: function (a, b, c) {
                    $('#post').button('reset');
//                    alert('something went wrong');
                    console.log(a);
                }
            })

        });
    }

</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-104924945-1', 'auto');
    ga('send', 'pageview');

</script>
</body>

</html>
