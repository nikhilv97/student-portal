<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="students,communicate,skills,peers,community,resume builder,resume,profile">
    <meta name="robots" content="index,follow">
    <meta name="description" content="A portal to connect with peers and explore.">

    {{--Facebook Open Graph--}}
    <meta property="og:title" content="ConnectInn">
    <meta property="og:type" content="article">
    <meta property="og:description" content="Connecting Young Professionals For a Productive Future">
    <meta property="og:image" content="http://www.connectinn.tk/bg_all.jpg">

    {{--Twitter Cards--}}
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="ConnectInn" />
    <meta name="twitter:description" content="Connecting Young Professionals For a Productive Future" />
    <meta name="twitter:image" content="http://www.connectinn.tk/bg_all.jpg" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>


    <!-- Styles -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <link href="/css/freelancer.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
          rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
          type="text/css">

    {{--STYLE SHEET--}}
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="shortcut icon" href="favicon.png">

    <script src="https://cdn.jsdelivr.net/select2/4.0.3/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">



</head>

<!-- Scripts -->
<script>
    window.Laravel =
        <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>

    var errorElement = function (message) {
            return "<span class='help-block' style='color: red'><strong>" + message + "</strong></span>"
        };
</script>

<style>
    @media screen and (min-width: 768px) {
        .modal-dialog {
            position: absolute;
            left: 50%;
            margin-left: -312px;
            height: 500px;
            top: 50%;
            margin-top: -250px;
        }
    }


</style>


<body style="padding: 0; margin: 0 ; max-width: 100%; overflow-x: hidden; " >
<div id="app">
    <div class="navbox">
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top"
             style="background: #fff;padding: 10px 10px; border-radius: 2px;box-shadow: 3px 4px 6px rgba(70,70,0,0.70);">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand"
                       style="color: #d43f3a;	font-family: 'Lobster', Georgia, Times, serif;font-size: 40px;line-height:22px;"
                       href="{{ url('/home') }}">ConnectInn</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">

                        @if (Auth::check())
                            <li>
                                <form class="navbar-form {{ $errors->has('filter') || $errors->has('query') ? ' has-error' : '' }}"
                                      role="search" method="get" action={{url('/search')}}>
                                    <div class="input-group add-on">
                                        <input class="form-control" placeholder="Search" name="query" id="query"
                                               type="text"
                                               value="{{isset($query) == null? "" : $query}}">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit" style="background: lightgray">
                                                <i
                                                        class="glyphicon glyphicon-search"></i></button>
                                        </div>
                                    </div>

                                </form>
                            </li>
                            <li><a href="{{url('/home')}}" style="color: firebrick">Home</a></li>
                            <li><a href="{{url('/myprofile')}}" style="color: firebrick">Profile</a></li>
                            <li><a href="{{url('/activities')}}" style="color: firebrick">Activities</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" style="color: firebrick" data-toggle="dropdown"
                                   role="button" aria-expanded="false">
                                    <img src="{{Auth::user()->profile_pic_url}}" height="25" width="25" alt="pic"
                                         class="img-circle"> <span id="user_name">{{ Auth::user()->name }}</span> <span
                                            class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a style="color: #2F4F4F" href="{{url('/me/resume')}}" target="_blank" style="color: firebrick">Download
                                            Resume</a>
                                    </li>
                                    <li>
                                        <a style="color: #2F4F4F" href="{{ url('/followers') }}">Followers</a>
                                    </li>

                                    <li role="separator" class="divider"></li>

                                    <li>
                                        <a style="color: #2F4F4F" href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();

    document.getElementById('logout-form').submit();">
                                            Logout <i class="fa fa-sign-out pull-right" aria-hidden="true"></i>

                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>

                        @endif
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->

            <!-- /.container-fluid -->
        </nav>
    </div>
</div>


<div style="padding-top: 100px">

    <div>

        @yield('content');


    </div>


</div>


</body>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="/js/jqBootstrapValidation.js"></script>
<script src="/js/contact_me.js"></script>

<!-- Theme JavaScript -->
<script src="/js/freelancer.min.js"></script>

<script>
    $(document).on('name_changed', function (event, name) {
        console.log(event);
        console.log('listen');
        $('#user_name').html(name);
    });
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-104924945-1', 'auto');
    ga('send', 'pageview');

</script>

@yield('scripts')

</html>
