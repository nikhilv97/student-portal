<div class="footer" style="background: #2F4F4F;">
    <div class="container">
        <div class="row" >
            <div class="col-md-5 col-xs-12 pull-left">
                <a href="#" role="button" class="btn btn-default"
                   style="background: #2F4F4F; color: lightgray; border: none">Feedback</a>
                <a href="#" role="button" class="btn btn-default"
                   style="background: #2F4F4F; color: lightgray; border: none">Privacy Policy</a>
                <a href="#" role="button" class="btn btn-default"
                   style="background: #2F4F4F; color: lightgray; border: none">Terms & Conditions</a>
            </div>
            <div class="row">
                <div style="text-align: right; color: gray;;" class="col-md-12 col-xs-12 pull-right">
                    &copy; 2018 ConnectInn
                </div>
            </div>
        </div>
    </div>
</div>


