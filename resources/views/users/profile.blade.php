@inject('userService', 'ConnectInn\Services\UserService')

@php
    if(!isset($activities)){
    $activities = $user->activities()->orderBy('created_at', 'desc')->get();
    }

    if(!isset($experiences)){
$experiences = $user->experiences;
}

 if(!isset($educations)){
$educations = $user->educations;
}

if(!isset($skills)){
$skills = $user->skills();
}

if(!isset($interests)){
$interests = $user->interests();
}

if(!isset($documents)){
$documents = $user->documents;
}

if (!isset($languages)) {
$languages = json_decode($user->languages, true);
}
@endphp

@extends('layouts.app')

@section('content')

    <style>
        ::-webkit-scrollbar {
            display: none;
        }
    </style>

    <div>

        @include('activities.addNewActivityModal')

    </div>

    <div>

        @include('activities.activitiesListModal')

    </div>

    <div>

        @include('users.educations.addEducationModal')

    </div>

    <div>

        @include('users.experiences.addExperienceModal')

    </div>


    <div>

        @include('users.skills.addSkillModal')

    </div>


    <div>

        @include('users.interests.addInterestModal')

    </div>

    <div>

        @include('users.documnets.addDocumentModal')

    </div>

    <div>

        @include('users.addLanguageModal')

    </div>

    <div>

        @include('users.addAboutModal')

    </div>

    <div>

        @include('users.addPhoneNoModal')

    </div>



    <div class="container-fluid">


        <div class="row" >
            <div class="col-md-12 col-xs-12">

                <div style="overflow: hidden;">
                    <div class="col-md-3" style="position: fixed;height:100vh;overflow-y: auto;" >
                        @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)

                        <div class="panel panel-default" id="resumeDetails">
                            <div class="heading-box">
                                <h3 align="center">Resume Details
                                </h3>
                            </div>

                            <div class="panel-body col-md-offset-1" >
                                <div class="row "> <i class="fa fa-arrow-right" aria-hidden="true"></i>  <a href="me/languages" data-toggle="modal"
                                                                                                           data-target="#add_language_modal"
                                                                                                           data-backdrop="false">Add Languages</a>
                                </div>

                                <div class="row"> <i class="fa fa-arrow-right" aria-hidden="true"></i>  <a href="#" data-toggle="modal"
                                                                                                           data-target="#add_about_modal"
                                                                                                           data-backdrop="false">Tell about yourself</a>
                                </div>

                                <div class="row"> <i class="fa fa-arrow-right" aria-hidden="true"></i>  <a href="#" data-toggle="modal"
                                                                                                           data-target="#add_phoneNo_modal"
                                                                                                           data-backdrop="false">Add phone number</a>
                                </div>
                                </div>
                        </div>
                        <br>
                        @endif
                        <div class="panel panel-default" id="education">
                            <div class="heading-box">
                                <h3 align="center">Educations
                                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                        <button class="btn btn-default pull-right" data-toggle="modal"
                                                data-target="#add_education_modal"
                                                data-backdrop="false" style="border: none">

                                            <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                               aria-hidden="true"></i>


                                        </button>
                                    @endif
                                </h3>
                            </div>

                            <div id="educationsListContainer" class="panel-body">
                                @if(count($educations) === 0)
                                    @include('placeholders.noEducation')
                                @else
                                    @include('users.educations.educationList')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xs-6"
                     style="left: 0;top: 0;height:100vh; overflow-y: scroll; margin-left: 25.5%; ">
                    <div class="row" id="top">
                        <div class="col-xs-12" style="background:#f05053; max-height: 500px; padding-top: 2em; padding-bottom: 2em">
                            <div class="row">
                                <div class="thumbnail col-md-2 col-md-offset-5 col-xs-6 col-xs-offset-3 ">
                                    <img src="{{$user->profile_pic_url}}"
                                         alt="Profile Pic Here">
                                    <div id="imageFormDiv" class="caption" style="display: none">
                                        <form class="form-horizontal" role="form" method="POST"
                                              enctype="multipart/form-data"
                                              action="{{ url('/profile-pic') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">

                                                <div class="col-md-12 col-xs-12">
                                                    <input type="file" class="form-control" name="image" required>

                                                    @if ($errors->has('image'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <input type="submit" value="Submit"/>
                                        </form>
                                    </div>
                                </div>
                                <div class="pull-right col-md-2">
                                    @if($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                        <i class="fa fa-pencil"
                                           style="float: right; font-size: 1.5em; color: white;; ; "
                                           onclick="enableDisable()"
                                           id="editbutton">
                                        </i>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="text-align: center">
                                    <div id="showDetailsDiv" style="padding-top: 10px">
                                        <div class="list-group">
                                            <div class="list-group-item" style="background:#f05053; border:  0;">
                                                <strong style="color: white">
                                                    <a href="#" id="userName"
                                                       style="color: white; font-size: large">{{$user->name}}</a>
                                                </strong>
                                            </div>
                                            {{--<div class="myListItem list-group-item" style="background:#b93838; border:  0;"><strong--}}
                                            {{--style="color: white">Branch : </strong>--}}
                                            {{--<a href="#" id="branch" style="color: white">{{$user->branch}}</a></div>--}}
                                            <div class="list-group-item myListItem"
                                                 style="background:#f05053; border:  0;">
                                                <strong style="color: white">
                                                    <a href="#" id="email" style="color: white">{{$user->email}}</a>
                                                </strong>
                                            </div>
                                            {{--<a href="#" id="branch" style="color: white">{{$user->branch}}</a></div>--}}
                                            <div class="list-group-item myListItem"
                                                 style="background:#f05053; border:  0;">
                                                <strong style="color: white">
                                                    <a href="#" id="userAbout"
                                                       style="color: white; font-style: italic"> {{$user->about}} </a>
                                                </strong>
                                            </div>
                                            @if($user->publicResumeLink())
                                                <div class="list-group-item myListItem"
                                                     style="background:#b93838; border:  0;">
                                                    <strong style="color: white">This is your public Resume Link</strong>
                                                    <strong style="color: white">
                                                        <a href="#" id="userResumeLink"
                                                           style="color: white; font-style: italic"> {{$user->publicResumeLink()}}  </a>
                                                    </strong>
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                               <div class="col-md-offset-5">
                                   @if(!is_null($rating = $user->avg_rating))

                                       @for($i=1; $i<= $rating; $i++)

                                           <i class="fa fa-star" style="color: yellow;" aria-hidden="true"></i>
                                       @endfor

                                       @for($i=$rating+1; $i<= 5; $i++)


                                           <i class="fa fa-star-o" style="color: yellow" aria-hidden="true"></i>
                                       @endfor

                                   @endif
                               </div>
                            </div>
                            <div class="row">
                                @if(!$userService->isLoggedIn($user))
                                    <div class="col-md-12" style="text-align: center">
                                        @if(\Illuminate\Support\Facades\Auth::user()->followers->contains($user->id))

                                            <div class="col-md-6 col-md-push-1" style="text-align: center">
                                                <a id="followButton" href="#" role="button"
                                                   style="color: white"
                                                ><i class="fa fa-user-times" data-toggle="tooltip" title="Unfollow"
                                                    aria-hidden="true" style="font-size: 2em"></i></a>
                                            </div>

                                        @else

                                            <div class="col-md-6 col-md-push-1" style="text-align: center">
                                                <a id="followButton" href="#" role="button"
                                                   style="color: white"
                                                ><i class="fa fa-user-plus" data-toggle="tooltip" title="Follow"
                                                    aria-hidden="true" style="font-size: 2em"></i></a>
                                            </div>

                                        @endif

                                        <div class="col-md-6 col-md-pull-1" style="text-align: center">
                                            <a href="#" role="button" data-toggle="modal"
                                               data-target="#activity_list_modal"
                                               data-backdrop="false"
                                               style="border: none; color: white; margin-left: 0.5em"
                                            ><i class="fa fa-envelope-open-o" data-toggle="tooltip"
                                                title="Invite to an Activity"
                                                aria-hidden="true" style="font-size: 2em"></i></a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row" id="experience">
                        <div class=" panel panel-default">
                            <div class=" heading-box">
                                <h3 align="center">Experiences
                                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                        <button class="btn btn-default pull-right" data-toggle="modal"
                                                data-target="#add_experience_modal"
                                                data-backdrop="false" style="border: none">

                                            <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                               aria-hidden="true"></i>


                                        </button>
                                    @endif
                                </h3>
                            </div>

                            <div id="experiencesListContainer" class="panel-body">
                                @if(count($experiences) === 0)
                                    @include('placeholders.noExperience')
                                @else
                                    @include('users.experiences.experienceList')
                                @endif
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row" id="activities">
                        <div class="panel panel-default">
                            <div class="heading-box">
                                <h3 align="center">Activities
                                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                        <button class="btn btn-default pull-right" data-toggle="modal"
                                                data-target="#add_activity_modal"
                                                data-backdrop="false" style="border: none">

                                            <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                               aria-hidden="true"></i>


                                        </button>
                                    @endif
                                </h3>
                            </div>

                            <div id="activitiesListContainer" class="panel-body">
                                @if(count($activities) === 0)
                                    @include('placeholders.noActivities')
                                @else
                                    @include('activities.activityList')
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row" id="documents">
                        <div class="panel panel-default">
                            <div class="heading-box">
                                <h4 align="center"> Documents/Certificates
                                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                        <button class="btn btn-default pull-right" data-toggle="modal"
                                                data-target="#add_documents_modal"
                                                data-backdrop="false" style="border: none">

                                            <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                               aria-hidden="true"></i>


                                        </button>
                                    @endif
                                </h4>
                            </div>

                            <div class="panel-body">
                                @if(count($documents) === 0)
                                    @include('placeholders.noDocuments')
                                @else
                                    @include('users.documnets.documentList')
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-3" style="position: fixed; overflow-y: scroll; margin-left: 74%; height: 80vh;">
                    {{--<div class="row">--}}
                    <div class="panel panel-default" id="skill">
                        <div class="heading-box">
                            <h2 align="center">Skills
                                @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                    <button class="btn btn-default pull-right" data-toggle="modal"
                                            data-target="#add_skill_modal"
                                            data-backdrop="false" style="border: none">
                                        <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                           aria-hidden="true"></i>
                                    </button>
                                @endif
                            </h2>
                        </div>

                        <div id="skillListContainer" class="panel-body">
                            @if(count($skills) === 0)
                                @include('placeholders.noSkill')
                            @else
                                @include('users.skills.skillList')
                            @endif
                        </div>
                    </div>
                 {{--   </div>--}}
                    <br>
                    {{--<div class="row">--}}
                    <div class="panel panel-default" id="interest">
                        <div class="heading-box">
                            <h2 align="center">Interests
                                @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                                    <button class="btn btn-default pull-right" data-toggle="modal"
                                            data-target="#add_interest_modal"
                                            data-backdrop="false" style="border: none">

                                        <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                           aria-hidden="true"></i>
                                    </button>
                                @endif
                            </h2>
                        </div>

                        <div id="interestListContainer" class="panel-body">
                            @if(count($interests) === 0)
                                @include('placeholders.noInterest')
                            @else
                                @include('users.interests.interestList')
                            @endif
                        </div>
                    </div>
                    {{--</div>--}}
                </div>

                <div class="background-image"></div>
            </div>

        </div>
    </div>


@endsection

@section('footer')
    @include('layouts.footer')
@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function () {
            $.fn.editable.defaults.mode = 'inline';
            $.fn.editable.defaults.ajaxOptions = {type: 'PATCH'};
            $('#editbutton').addClass('fa fa-pencil');

            $('#userName').editable({
                type: 'text',
                title: 'This a test',
                url: 'user/updated',
                name: 'name',
                disabled: true,
                send: 'always',
                success: function (data) {
                    console.log(data);
                    $( document ).trigger( 'name_changed', data );
                }
            });
//
        });

        jQuery(document).on('name_changed', function (event, name) {
            console.log(name);
        });


        function enableDisable() {
            console.log('asdas');
            var button = $('#editbutton');
            if (button.hasClass('fa fa-pencil')) {
                button.removeClass('fa fa-pencil');
                button.addClass('fa fa-times');
            }
            else {
                button.removeClass('fa fa-times');
                button.addClass('fa fa-pencil');
            }
            $('#userName').editable('toggleDisabled');
            $('#imageFormDiv').delay(200).slideToggle('slow');
        }

        var followUrl = "{{url('/followers/'.$user->id.'/attach')}}";
        var unfollowUrl = "{{url('/followers/'.$user->id.'/detach')}}";

                @if(\Illuminate\Support\Facades\Auth::user()->followers->contains($user->id))

        var url = unfollowUrl;
                @else

        var url = followUrl;

                @endif

        var followI = "<i class='fa fa-user-plus' data-toggle='tooltip' title='Follow' aria-hidden='true' style='font-size: 2em'></i>";
        var unfollowI = "<i class='fa fa-user-times' data-toggle='tooltip' title='Unfollow' aria-hidden='true' style='font-size: 2em'></i>";


        $('#followButton').on('click', function () {
            console.log('clicked');
            $.ajax(url, {
                success: function (res, status) {
                    console.log('don');

                },

                failure: function (res, status) {

                }
            })
                .done(function (response) {
                    $('#followButton').empty();

                    if (url === followUrl) {
                        $('#followButton').append(unfollowI);
                        url = unfollowUrl;
                    } else {
                        $('#followButton').append(followI);
                        url = followUrl;
                    }
                })

                .fail(function (jqXhr, textStatus) {

                })
        });

        $('#copyButton').on('click', function () {
            var copyTextarea = document.querySelector('#userResumeLink');
            copyTextarea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }
        });

    </script>
@endsection