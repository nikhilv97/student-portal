@php
    if(!isset($users)){
    $users= $activity->users;

    }
@endphp


<ul class="list-group">
    @foreach($users as $user)
        <div class="panel-default">
            <div class="panel-body">
                <div class="">
                <li class=" resultbox list-group-item col-md-12">
                    @include('users.userListItem')
                </li>
                </div>
            </div>
        </div>

    @endforeach
</ul>