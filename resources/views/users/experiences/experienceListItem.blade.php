
<div>

    @include('users.experiences.addExperienceDocumentModal')

</div>



<li id = "experienceItem{{$experience->id}}"class="list-group-item">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 ">
            <div class="row">
                <strong><i class="fa fa-university " style="font-size:1em; color: #444444"
                           aria-hidden="true"></i>{{$experience->organisation_name}}</strong>

                @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                    <div class="pull-right">
                        <button class="btn btn-default" data-toggle="modal"
                                data-target="#add_experience_document_modal"
                                data-backdrop="false" style="border: none">

                            <i class="fa fa-plus " style="font-size:1em; color: #444444"
                               aria-hidden="true"></i>


                        </button>
                        <i class="fa fa-times " onclick="deleteExperience{{$experience->id}}()" style="color: darkred;"
                           aria-hidden="true"></i>
                    </div>
                @endif


            </div>

            <div class="row">
                <strong style="font-style: normal; color: darkgray">{{$experience->designation}}</strong>
                <div class="pull-right" style="font-style: italic; color: darkgray">( {{$experience->from}} -
                    @if(is_null($experience->to))
                        currently ongoing )
                @else
                    {{$experience->to}} )
                @endif
                </div></div>
            <br>

            <div class="row">
                <strong><em>
                        {{$experience->description}}
                    </em></strong>
            </div>

          @foreach($experience->documents as $document)

                <div class="row">
                    <div class="thumbnail ">
                        <img src="{{$document->url}}"
                             alt="Attach Document Here">
                    </div>

                </div>

              @endforeach
        </div>
        </div>
    </div>
</li>

<script>
    function deleteExperience{{$experience->id}}() {
        $.ajax('experience/{{$experience->id}}', {
            method: 'DELETE',
            data: {
                _token: '{{csrf_token()}}'
            }
        })
            .done(function (response) {
                console.log(response)
                $('#experienceItem{{$experience->id}}').remove()
            })

            .fail(function (jqXhr, textStatus) {
                console.log(jqXhr)
            })
    }
</script>