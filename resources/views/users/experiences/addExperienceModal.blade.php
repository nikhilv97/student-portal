<div id="add_experience_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Experience
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addExperienceForm" action="{{url('me/experiences')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Name of Organisation : </label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" placeholder="Company's name" class="form-control" name="name" value="{{ old('name') }}"
                                               required autofocus>

                                        <div id="nameExperienceError"></div>
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description" class="col-md-4 control-label">Description : </label>

                                    <div class="col-md-6">
                                        <textarea id="description" type="text" placeholder="Share the work experience (optional) " class="form-control" name="description" value="{{ old('description') }}"
                                                   style="max-width:100% " autofocus></textarea>

                                        <div id="descriptionExperienceError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                    <label for="location" class="col-md-4 control-label">Location : </label>

                                    <div class="col-md-6">
                                        <textarea id="location" type="text" placeholder="add location " class="form-control" name="location" value="{{ old('location') }}"
                                                  style="max-width:100% " autofocus></textarea>

                                        <div id="locationExperienceError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                                    <label for="designation" class="col-md-4 control-label">Designation : </label>

                                    <div class="col-md-6">
                                        <input id="designation" type="text" placeholder="eg: Intern, Management Head, Software Developer, etc." class="form-control" name="designation" value="{{ old('designation') }}"
                                               required autofocus>

                                        <div id="designationExperienceError"></div>
                                    </div>

                                </div>



                                <div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                                    <label for="e_year" class="col-md-4 control-label">From :</label>

                                    <div class="col-md-6 ">
                                        <select id="e_year" class="form-control custom-select" name="from" required>
                                        </select>
                                        <div id="fromExperienceError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-5">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input id="e_ongoing" type="checkbox" class="form-check-input">
                                                Ongoing
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                                    <label for="e_to_year" class="col-md-4 control-label">To :</label>

                                    <div class="col-md-6">
                                        <select id="e_to_year" class="form-control custom-select" name="to">
                                        </select>
                                        <div id="toExperienceError"></div>
                                    </div>
                                </div>


                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postExperience" onclick="addExperience() " class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    var start = 1900;
    var end = new Date().getFullYear();
    var options = "";
    for(var year = end ; year >= start; year--){
        options += "<option value" + year + ">"+ year +"</option>";
    }
    document.getElementById("e_year").innerHTML = options;
    document.getElementById("e_to_year").innerHTML = options;

    function addExperience() {
        $('#postExperience').button('loading');

        $('#addExperienceForm').off('submit').submit(function (event) {
            event.preventDefault();

            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    //
                },

                failure: function (a, b, c) {
                    $('#postExperience').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })
                .done(function (response) {
                    $('#postExperience').button('reset');
                    $('#add_experience_modal').modal('hide');

                    var nodes = $('#experiencesListContainer > ul').length;

                    if (nodes === 0) {
                        $('#experiencesListContainer').empty();
                        $('#experiencesListContainer').append(response);
                    } else {
                        $('#experienceList').prepend(response);
                    }
                })

                .fail(function (jqXhr, textStatus) {
                    $('#postExperience').button('reset');

                    var errorJson = jQuery.parseJSON(jqXhr.responseText.trim());

                    if (errorJson.hasOwnProperty('name')) {
                        if ($('#nameExperienceError').is(':empty')) {
                            $('#nameExperienceError').append(errorElement(errorJson.name[0]));
                        }
                    } else {
                        $('#nameExperienceError').empty();
                    }

                    if (errorJson.hasOwnProperty('description')) {
                        if ($('#descriptionExperienceError').is(':empty')) {
                            $('#descriptionExperienceError').append(errorElement(errorJson.description[0]));
                        }
                    } else {
                        $('#descriptionExperienceError').empty();
                    }

                    if (errorJson.hasOwnProperty('location')) {
                        if ($('#locationExperienceError').is(':empty')) {
                            $('#locationExperienceError').append(errorElement(errorJson.location[0]));
                        }
                    } else {
                        $('#locationExperienceError').empty();
                    }

                    if (errorJson.hasOwnProperty('designation')) {
                        if ($('#designationExperienceError').is(':empty')) {
                            $('#designationExperienceError').append(errorElement(errorJson.designation[0]));
                        }
                    } else {
                        $('#designationExperienceError').empty();
                    }

                    if (errorJson.hasOwnProperty('from')) {
                        if ($('#fromExperienceError').is(':empty')) {
                            $('#fromExperienceError').append(errorElement(errorJson.from[0]));
                        }
                    } else {
                        $('#fromExperienceError').empty();
                    }

                    if (errorJson.hasOwnProperty('to')) {
                        if ($('#toExperienceError').is(':empty')) {
                            $('#toExperienceError').append(errorElement(errorJson.to[0]));
                        }
                    } else {
                        $('#toExperienceError').empty();
                    }
                })

        });
    }

    $('#e_ongoing').change(function () {
        if (!this.checked) {
            $('#e_to_year').prop('disabled', '');
        } else {
            $('#e_to_year').prop('disabled', 'disabled');
        }
    });

</script>