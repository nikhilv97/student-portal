@php
    if(!isset($experiences)){
    $experiences= $user->experiences;

    }
@endphp


<ul id="experienceList" class="list-group">
    @foreach($experiences as $experience)
        <div class="panel-default box">
            <div class="panel-body">
                    @include('users.experiences.experienceListItem')
            </div>
        </div>
    @endforeach
</ul>
