<ul id="skillsList" class="list-group">
    @foreach($skills as $skill)
            @include('users.skills.skillListItem')
    @endforeach
</ul>