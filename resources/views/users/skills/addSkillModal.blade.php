<div id="add_skill_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Skill
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addSkillForm" action="{{url('me/attributes')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('skill') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-3">
                                        <input id="skill" type="text" placeholder="your skill" class="form-control" name="value" value="{{ old('skill') }}"
                                               required autofocus>

                                        @if ($errors->has('skill'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('skill') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <input type="hidden" value="skill" name="type">

                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postSkill" onclick="addSkill() " class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>

    function addSkill() {
        $('#postSkill').button('loading');

        $('#addSkillForm').off('submit').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    $('#postSkill').button('reset');
                    $('#noSkill').remove();
                    $('#add_skill_modal').modal('hide');

                    var nodes = $('#skillListContainer > ul').length;
                    console.log(nodes);

                    if (nodes === 0) {
                        $('#skillListContainer').append(response);
                    } else {
                        $('#skillsList').prepend(response);
                    }

                },

                failure: function (a, b, c) {
                    $('#postSkill').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })

        });
    }



</script>