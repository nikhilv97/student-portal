<li id="documentItem{{$document->id}}" class="list-group-item">
    <div  class="container-fluid">
        <div class="row">
            <div  class="col-md-12 col-xs-12">
                <div class="row">
                    <strong><i class="fa fa-image " style="font-size:1em; color: #444444"
                               aria-hidden="true"></i> {{$document->title}}</strong>
                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                        <i class="fa fa-times pull-right" onclick="deleteDocument{{$document->id}}()"
                           style="color: darkred;"
                           aria-hidden="true"></i>
                    @endif
                </div>
                <br>

                <div class="row">
                    <strong style="font-style: italic; color: black"> {{$document->description}} </strong>
                </div>
                <br>

                <div class="row">
                    <div class="thumbnail ">
                        <img src="{{$document->url}}"
                             alt="Attach Document Here">
                    </div>

                </div>
            </div>
        </div>
    </div>
</li>


<script>
    function deleteDocument{{$document->id}}() {
        $.ajax('documents/{{$document->id}}', {
            method: 'DELETE',
            data: {
                _token: '{{csrf_token()}}'
            }
        })
            .done(function (response) {
                console.log(response)
                $('#documentItem{{$document->id}}').remove()
            })

            .fail(function (jqXhr, textStatus) {
                console.log(jqXhr)
            })
    }
</script>