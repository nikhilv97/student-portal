@php
    if(!isset($documents)){
    $documents= $user->documents;

    }
@endphp


<ul id="documentList" class="list-group">
    @foreach($documents as $document)

        <div class="panel-default box">
            <div class="panel-body">
            @include('users.documnets.documentListItem')
            </div>
        </div>

    @endforeach
</ul>