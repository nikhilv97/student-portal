<div id="userContent" class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-xs-12">

            <div class="col-md-1 col-xs-3">
                <img src="{{$user->profile_pic_url}}" height="50" width="50" alt="pic"
                     class="img-circle">
            </div>


            <div class="col-md-8 col-xs-9">
                <a href="/user/{{$user->id}}/timeline">
                    <h4>
                        <span id="nameContent{{$user->id}}" style="color: #1b6d85">{{$user->name}}</span>
                    </h4>
                </a>
            </div>


            @if(\Illuminate\Support\Facades\Auth::user()->followers->contains($user->id))
                <div class="col-md-1 col-md-offset-2 col-xs-1 pull-right">
                    <a href="{{url('/followers/'.$user->id.'/detach')}}" role="button" class="pull-right">Unfollow</a>
                </div>
            @else
                <div class="col-md-1 col-xs-1 col-md-offset-2 pull-right">
                    <a href="{{url('/followers/'.$user->id.'/attach')}}" role="button" class="pull-right">Follow</a>
                </div>
            @endif

        </div>

    </div>
    <div class="row">
        <div class="col-xs-12">
            <h5 style="color: #2C3E50"><strong>Email</strong>: <span id="emailContent{{$user->id}}">{{$user->email}}</span></h5>

            @if(!($user->skills()->isEmpty()))
                <h5 style="color: #2C3E50"><strong>Skills: </strong>
                    @foreach($user->skills() as $skill)
                        <span class="skillInterest{{$user->id}}">{{$skill->value}}</span>
                    @endforeach
                </h5>
            @endif

            @if(!($user->interests()->isEmpty()))
                <h5 style="color: #2C3E50"><strong>Interests: </strong>
                    @foreach($user->interests() as $interest)
                        <span id="interestContent{{$user->id}}">{{$interest->value}}</span>
                    @endforeach
                </h5>
            @endif

        </div>
    </div>
</div>

@if(isset($query))
<script>
    jQuery(document).ready(function () {
        changeEmail{{$user->id}}();
        changeInterest{{$user->id}}();
        changeSkill{{$user->id}}();
        changeName{{$user->id}}();
    });

    function changeEmail{{$user->id}}() {
        let src_str = $("#emailContent" + "{{$user->id}}").html();
        let term = "{{$query}}";
        term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
        let pattern = new RegExp("(" + term + ")", "gi");

//        src_str = src_str.replace(pattern, "<mark>$1</mark>");
//        src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

        src_str = src_str.replace(term, "<span style='color: yellow; background: yellowgreen'>" + term + "</span>");

        $("#emailContent" + "{{$user->id}}").html(src_str);
    }

    function changeSkill{{$user->id}}() {
        let src_str = $("#skillInterest" + "{{$user->id}}").html();
        let term = "{{$query}}";
        term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
        let pattern = new RegExp("(" + term + ")", "gi");

//        src_str = src_str.replace(pattern, "<mark>$1</mark>");
//        src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

        src_str = src_str.replace(term, "<span style='color: yellow; background: yellowgreen'>" + term + "</span>");

        $("#skillInterest" + "{{$user->id}}").html(src_str);
    }

    function changeInterest{{$user->id}}() {
        let src_str = $("#interestContent" + "{{$user->id}}").html();
        let term = "{{$query}}";
        term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
        let pattern = new RegExp("(" + term + ")", "gi");

//        src_str = src_str.replace(pattern, "<mark>$1</mark>");
//        src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

        src_str = src_str.replace(term, "<span style='color: yellow; background: yellowgreen'>" + term + "</span>");

        $("#interestContent" + "{{$user->id}}").html(src_str);
    }

    function changeName{{$user->id}}() {
        let src_str = $("#nameContent" + "{{$user->id}}").html();
        let term = "{{$query}}";
        term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
        let pattern = new RegExp("(" + term + ")", "gi");

//        src_str = src_str.replace(pattern, "<mark>$1</mark>");
//        src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

        src_str = src_str.replace(term, "<span style='color: yellow; background: yellowgreen'>" + term + "</span>");

        $("#nameContent" + "{{$user->id}}").html(src_str);
    }
</script>
    @endif