@php
    $user_language_json = $user->language;
    $languages = json_decode($user_language_json, true);
    //dd($languages);

@endphp

<div id="add_language_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Language
                            </h3>
                        </div>
                        <div class="modal-body">
                            @if(!is_null($languages))
                                <ul>
                                    @foreach($languages as $language)
                                        <li>{{$language}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <form id="addLanguageForm" action="{{url('/me/languages')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}
                                <div class="form-group{{ $errors->has('language') ? ' has-error' : '' }}">


                                    <div class="col-md-6 col-md-offset-3">
                                        <input id="language" type="text" placeholder="your language"
                                               class="form-control" name="language" value="{{ old('language') }}"
                                               required autofocus>

                                        @if ($errors->has('language'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('language') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('fluency') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-3">
                                        <select id="fluency" class="form-control custom-select" name="fluency">
                                            <option value="elementary">Elementary</option>
                                            <option value="professional">Professional</option>
                                            <option value="full_professional">Full Professional</option>
                                            <option value="bilingual">Bilingual</option>
                                        </select>

                                        @if ($errors->has('fluency'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('fluency') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postLanguage" class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>

    function addLanguage() {
        $('#postLanguage').button('loading');

        $('#add_language_modal').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (response, status, jq) {
                    $('#postLanguage').button('reset');
                    $('#add_language_modal').modal('hide');

                    $('#languageList').prepend(response);
                },

                failure: function (a, b, c) {
                    $('#postLanguage').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })

        });
    }


</script>