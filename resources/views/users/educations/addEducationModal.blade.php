<div id="add_education_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Education
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addEducationForm" action="{{url('me/educations')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Name of the Institute : </label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" placeholder="School/College name" class="form-control" name="name" value="{{ old('name') }}"
                                               required autofocus>

                                        <div id="nameEducationError"></div>
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('degree') ? ' has-error' : '' }}">
                                    <label for="degree" class="col-md-4 control-label">Degree : </label>

                                    <div class="col-md-6">
                                        <input id="degree" type="text" placeholder="eg: Highschool, Inyermediate, B.tech, etc." class="form-control" name="degree" value="{{ old('degree') }}"
                                               required autofocus>

                                        <div id="degreeEducationError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                    <label for="location" class="col-md-4 control-label">Location : </label>

                                    <div class="col-md-6">
                                        <textarea id="location" type="text" placeholder="add location " class="form-control" name="location" value="{{ old('location') }}"
                                                  style="max-width:100% " autofocus></textarea>

                                        <div id="locationEducationError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('grade') ? ' has-error' : '' }}">
                                    <label for="grade" class="col-md-4 control-label">Grade : </label>

                                    <div class="col-md-6">
                                        <input id="grade" type="text" placeholder="eg: 9, 99.2" class="form-control" name="grade" value="{{ old('grade') }}"
                                               required autofocus>

                                        <div id="gradeEducationError"></div>
                                    </div>

                                </div>
                                <div class="form-group{{ $errors->has('grade_type') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-5">
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-primary active">
                                                <input type="radio" name="grade_type" value="cgpa" id="cgpa" autocomplete="off" checked> CGPA
                                            </label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="grade_type" value="percentage" id="percentage" autocomplete="off"> Percentage
                                            </label>
                                        </div>

                                    <div id="gradeTypeEducationError"></div>
                                    </div>

                                </div>


                                <div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
                                    <label for="year" class="col-md-4 control-label">From :</label>

                                    <div class="col-md-6 ">
                                        <select id="year" class="form-control custom-select" name="from" required>
                                        </select>
                                        <div id="fromEducationError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-5">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input id="ongoing" type="checkbox" class="form-check-input">
                                                Ongoing
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">
                                    <label for="to_year" class="col-md-4 control-label">To :</label>

                                    <div class="col-md-6">
                                        <select id="to_year" class="form-control custom-select" name="to">
                                        </select>
                                        <div id="toEducationError"></div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('official_body') ? ' has-error' : '' }}">
                                    <label for="official_body" class="col-md-4 control-label">Official Body : </label>

                                    <div class="col-md-6">
                                        <input id="official_body" type="text" placeholder="eg: CBSE, ISC, UP Board etc." class="form-control" name="official_body" value="{{ old('official_body') }}"
                                               required autofocus>

                                        <div id="officialBodyEducationError"></div>
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postEducation" onclick="addEducation() " class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    var start = 1900;
    var end = new Date().getFullYear();
    var options = "";
    for(var year = end ; year >= start; year--){
        options += "<option value" + year + ">"+ year +"</option>";
    }
    document.getElementById("year").innerHTML = options;
    document.getElementById("to_year").innerHTML = options;

    function addEducation() {
        $('#postEducation').button('loading');

        $('#addEducationForm').off('submit').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    console.log(status);
                },

                failure: function (a, b, c) {
                    $('#postEducation').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })
                .done(function (response) {
                    $('#postEducation').button('reset');
                    $('#add_education_modal').modal('hide');

                    var nodes = $('#educationsListContainer > ul').length;

                    if (nodes === 0) {
                        $('#educationsListContainer').empty();
                        $('#educationsListContainer').append(response);
                    } else {
                        $('#educationList').prepend(response);
                    }
                })

                .fail(function (jqXhr, textStatus) {
                    $('#postEducation').button('reset');
                    var errorJson = jQuery.parseJSON(jqXhr.responseText.trim());
                    console.log(errorJson);

                    if (errorJson.hasOwnProperty('name')) {
                        if ($('#nameEducationError').is(':empty')) {
                            $('#nameEducationError').append(errorElement(errorJson.name[0]));
                        }
                    } else {
                        $('#nameEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('degree')) {
                        if ($('#degreeEducationError').is(':empty')) {
                            $('#degreeEducationError').append(errorElement(errorJson.degree[0]));
                        }
                    } else {
                        $('#degreeEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('location')) {
                        if ($('#locationEducationError').is(':empty')) {
                            $('#locationEducationError').append(errorElement(errorJson.location[0]));
                        }
                    } else {
                        $('#locationEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('grade')) {
                        if ($('#gradeEducationError').is(':empty')) {
                            $('#gradeEducationError').append(errorElement(errorJson.grade[0]));
                        }
                    } else {
                        $('#gradeEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('grade_type')) {
                        if ($('#gradeTypeEducationError').is(':empty')) {
                            $('#gradeTypeEducationError').append(errorElement(errorJson.grade_type[0]));
                        }
                    } else {
                        $('#gradeTypeEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('degree')) {
                        if ($('#degreeEducationError').is(':empty')) {
                            $('#degreeEducationError').append(errorElement(errorJson.degree[0]));
                        }
                    } else {
                        $('#degreeEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('from')) {
                        if ($('#fromEducationError').is(':empty')) {
                            $('#fromEducationError').append(errorElement(errorJson.from[0]));
                        }
                    } else {
                        $('#fromEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('to')) {
                        if ($('#toEducationError').is(':empty')) {
                            $('#toEducationError').append(errorElement(errorJson.to[0]));
                        }
                    } else {
                        $('#toEducationError').empty();
                    }

                    if (errorJson.hasOwnProperty('official_body')) {
                        if ($('#officialBodyEducationError').is(':empty')) {
                            $('#officialBodyEducationError').append(errorElement(errorJson.official_body[0]));
                        }
                    } else {
                        $('#officialBodyEducationError').empty();
                    }
                })

        });
    }

    $('#ongoing').change(function () {
       if (!this.checked) {
           $('#to_year').prop('disabled', '');
       } else {
           $('#to_year').prop('disabled', 'disabled');
       }
    });

</script>