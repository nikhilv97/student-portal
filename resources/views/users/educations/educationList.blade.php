@php
    if(!isset($educations)){
    $educations= $user->educations;

    }
@endphp


<ul id="educationList" class="list-group">
    @foreach($educations as $education)

        @include('users.educations.educationListItem')

    @endforeach
</ul>