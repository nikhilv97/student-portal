<div>

    @include('users.educations.addEducationDocumentModal')

</div>


<li id="educationItem{{$education->id}}" class="list-group-item">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 ">
                <div class="row">

                        <strong><i class="fa fa-book " style="font-size:1em; color: #444444"
                                   aria-hidden="true"></i> {{$education->degree}}</strong>


                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                        <div class="pull-right ">


                                <button class="btn btn-default " data-toggle="modal"
                                        data-target="#add_education_document_modal"
                                        data-backdrop="false" style="border: none">

                                    <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                       aria-hidden="true"></i>


                                </button>



                                <i class="fa fa-times " onclick="deleteEducation{{$education->id}}()"
                                   style="color: darkred;"
                                   aria-hidden="true"></i>

                        </div>
                    @endif
                </div>

            <br>
            <div class="row">
                <strong> <i class="fa fa-university " style="font-size:1em; color: #444444"
                            aria-hidden="true"></i> {{$education->name}}</strong>
                <div class="pull-right" style="font-style: italic; color: darkgray">( {{$education->from}}-
                    @if($education->to == null)
                        currently persuing )

                    @else
                        {{$education->to}} )
                    @endif
                </div>
            </div>

            <div class="row">
                <strong style="font-style: italic; color: darkgray">( {{$education->location}} )</strong>
            </div>

            <div class="row">
                <strong style="font-style: normal; color: darkgray">{{$education->official_body}}</strong>
            </div>
            <br>

            <div class="row">
                <strong>Scored : {{$education->grade}} {{$education->grade_type === 'cgpa' ? 'CGPA' : '%'}} </strong>
            </div>

            @foreach($education->documents as $document)

                <div class="row">
                    <div class="thumbnail ">
                        <img src="{{$document->url}}"
                             alt="Attach Document Here">
                    </div>

                </div>

            @endforeach
            </div>
        </div>
    </div>
</li>

<script>
    function deleteEducation{{$education->id}}() {
        $.ajax('education/{{$education->id}}', {
            method: 'DELETE',
            data: {
                _token: '{{csrf_token()}}'
            }
        })
            .done(function (response) {
                console.log(response)
                $('#educationItem{{$education->id}}').remove()
            })

            .fail(function (jqXhr, textStatus) {
                console.log(jqXhr)
            })
    }
</script>
