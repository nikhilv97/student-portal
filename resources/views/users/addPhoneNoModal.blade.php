<div id="add_phoneNo_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="removeErrmsg()">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add About
                            </h3>
                        </div>
                        <div class="modal-body">
                            {{-- <ul>
                                 @foreach($languages as $language)
                                 <li>{{$language}}</li>
                                     @endforeach
                             </ul>--}}
                            <form id="addPhoneNumberForm" action="{{url('user/updated')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}
                                {{method_field('PATCH')}}

                                <div class="form-group{{ $errors->has('phoneNo') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-3">
                                        <input type="hidden" name="name" value="phone_number">
                                        <input id="phoneNo" type="text" placeholder="write yor contact number with country code" class="form-control" name="value" value="{{Auth::user()->phone_number}}"
                                                  required autofocus>

                                        <div id="phoneNumberError"></div>

                                        @if ($errors->has('phoneNo'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phoneNo') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postPhoneNumber" class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>

    function removeErrmsg() {
        $('#phoneNumberError').empty();
    }
    $('#postPhoneNumber').on('click',function (event) {
        event.preventDefault();
        var num = $('#phoneNo').val();
        var filter = /\+[0-9]{12}/;
        if(filter.test(num)){
            $('#addPhoneNumberForm').submit();
        }
        else{
            $('#phoneNumberError').empty();
            $('#phoneNo').empty();
            $('#phoneNumberError').append(errorElement('Number should be in +91{10 digit} format'));
        }





    });

</script>

