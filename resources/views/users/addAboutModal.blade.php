<div id="add_about_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add About
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addAboutForm" action="{{url('user/updated')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}
                                {{method_field('PATCH')}}

                                <div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-3">
                                        <input type="hidden" name="name" value="about">
                                        <textarea id="about" type="text" placeholder="write about yourself" class="form-control" name="value"
                                                  required autofocus>{{Auth::user()->about}}</textarea>

                                        @if ($errors->has('about'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postAbout" class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
