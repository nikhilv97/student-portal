<div id="add_interest_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Interst
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addInterestForm" action="{{url('me/attributes')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">

                                    <div class="col-md-6 col-md-offset-3">
                                        <input id="interest" type="text" placeholder="your interest" class="form-control" name="value" value="{{ old('value') }}"
                                               required autofocus>

                                        @if ($errors->has('value'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <input type="hidden" value="interest" name="type">

                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postInterest" onclick="addInterest() " class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>

    function addInterest() {
        $('#postInterest').button('loading');

        $('#addInterestForm').off('submit').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    $('#postInterest').button('reset');
                    $('#add_interest_modal').modal('hide');

                    var nodes = $('#interestListContainer > ul').length;

                    if (nodes === 0) {
                        $('#interestListContainer').empty();
                        $('#interestListContainer').append(response);
                    } else {
                        $('#interestsList').prepend(response);
                    }
                },

                failure: function (a, b, c) {
                    $('#postInterest').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })

        });
    }


</script>