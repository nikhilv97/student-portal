@php
    if(!isset($interests)){
    $interests= $user->interests();

    }
@endphp


<ul id="interestsList" class="list-group" style="padding-bottom: 5px">
    @foreach($interests as $interest)
        @include('users.interests.interestListItem')
    @endforeach
</ul>