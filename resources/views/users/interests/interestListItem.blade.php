<li id="interestItem{{$interest->id}}" class="list-group-item col-md-12">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <strong>{{$interest->value}}</strong>
                    @if ($user->id ==\Illuminate\Support\Facades\Auth::user()->id)
                        <i class="fa fa-times pull-right" onclick="deleteInterest{{$interest->id}}()"
                           style="color: darkred;" aria-hidden="true"></i>
                    @endif
                </div>
            </div>
        </div>
    </div>
</li>

<script>
    function deleteInterest{{$interest->id}}() {
        $.ajax('attributes/{{$interest->id}}', {
            method: 'DELETE',
            data: {
                _token: '{{csrf_token()}}'
            }
        })
            .done(function (response) {
                console.log(response)
                $('#interestItem{{$interest->id}}').remove()
            })

            .fail(function (jqXhr, textStatus) {
                console.log(jqXhr)
            })
    }
</script>