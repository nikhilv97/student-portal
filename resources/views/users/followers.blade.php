@php
    if(!isset($users)){
    $users= $activity->users;

    }

@endphp


@if (count($users) === 0)
    @include('placeholders.noFollowers')
@else
    @extends('layouts.app')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('users.userList')

                <div class="background-image"></div>
            </div>
        </div>
        <div>

@endsection
{{--@section('footer')
    @include('layouts.footer')
@endsection--}}
@endif

{{--@section('footer')--}}
    {{--@include('layouts.footer')--}}
{{--@endsection--}}
