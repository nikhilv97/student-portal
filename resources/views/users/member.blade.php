@inject('userService', 'ConnectInn\Services\UserService')

<div>
    @include('activities.activityMemberRateModal')
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <img src="{{$user->profile_pic_url}}" height="50" width="50" alt="pic"
             class="img-circle">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
           href="#collapseThree{{ $user->id }}"
           aria-expanded="false" aria-controls="collapseThree">
            {{ $user->name }}
        </a>
        @if(!$userService->isLoggedIn($user))
            <div class="pull-right dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <i class="fa fa-caret-down" aria-hidden="true" style="font-size: 1.5em"></i>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ url('/user/'.  $user->id  . '/timeline') }}">View Profile</a>

                    </li>
                    @if(\Illuminate\Support\Facades\Auth::user()->followers->contains($user->id))
                        <li>
                            <a href="{{ url('/followers/' . $user->id . '/detach') }}">
                                Unfollow
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="{{ url('/followers/' . $user->id . '/attach') }}">
                                Follow
                            </a>
                        </li>
                    @endif
                    @if($activity->users->contains(Auth::id()))
                        <li>
                            <a href="#" data-toggle="modal" data-target="#rate_modal" data-backdrop="false">
                                Rate
                            </a>
                        </li>
                    @endif


                    @if($userService->isLoggedIn($activity->owner()))
                        <li role="separator" class="divider"></li>

                        <li>
                            <a href="{{ url('/activity/'.$activity->id.'/users/'.$user->id.'/detach') }}">
                                Remove
                            </a>
                        </li>

                    @endif


                </ul>
            </div>



        @endif
    </div>
    <ul class="list-group">
        <div id="collapseThree{{ $user->id }}" class="panel-collapse collapse" role="tabpanel"
             aria-labelledby="headingThree">
            <li class="list-group-item">
                <h5><strong>Email</strong>: {{$user->email}}</h5>
                @php
                    $workingAs = $activity->users()->where('email', $user->email)->value('working_as');
                    $type = $activity->users()->where('email', $user->email)->value('type');
                @endphp
                @if(!is_null($workingAs))
                    <h5><strong>Working As :</strong>{{$workingAs}} </h5>
                @else
                    <h5><strong>Working As :</strong>{{$type}} </h5>

                @endif


                <h5><strong>Branch:</strong> {{$user->branch}}</h5>

                @if(!is_null($user->skills()) && count($user->skills()) !== 0)
                    <h5><strong>Skills: </strong>
                        @foreach($user->skills() as $skill)
                            {{$skill->value}}
                        @endforeach
                    </h5>
                @endif

                @if(!is_null($user->interests()) && count($user->interests()) !== 0)
                    <h5><strong>Interests: </strong>
                        @foreach($user->interests() as $interest)
                            {{$interest->value}}
                        @endforeach
                    </h5>
                @endif

                @if(!is_null($rating = $user->avgRatingInActivity($activity->id)))
                    <h5><strong>Rating: </strong>
                        @for($i=1; $i<= $rating; $i++)

                            <i class="fa fa-star" style="color: yellow;" aria-hidden="true"></i>
                        @endfor

                        @for($i=$rating+1; $i<= 5; $i++)


                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        @endfor

                        @endif


                    </h5>
            </li>
        </div>
    </ul>
</div>
