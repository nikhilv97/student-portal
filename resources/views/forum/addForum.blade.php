<form class="form-horizontal" role="form" method="POST" action="{{url('/forums')}}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="title" class="col-md-4 control-label">Title:</label>

        <div class="col-md-6">
            <input id="title" type="text" class="form-control" name="title"
                   placeholder="Title of activity"
                   autofocus required>

            @if ($errors->has('title'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
        <label for="description" class="col-md-4 control-label">Description:</label>

        <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="desc"
                                          placeholder="Description of activity" required></textarea>

            @if ($errors->has('desc'))
                <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label for="isPublic" class="col-md-4 control-label">Is Public :</label>

        <div class="col-md-6">
             <input id="isPublic" type="radio">
        </div>
    </div>


</form>
