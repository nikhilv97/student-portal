

<div class="panel panel-default activitybox">
     <div>
         <div class="row">
              <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-8">
                          <img src="" height="50" width="50" alt="pic"
                               class="img-circle">
                          <a class="username" href="/forum/{{$forum->id}}"></a>
                      </div>
                      <div>
                          <hr style="border: 1px solid darkgray">
                      </div>

                      @if(Auth::id()==$forum->owner_id)
                          <div class="col-md-4">
                              <div class="row pull-right">
                                  <div class="col-md-2">

                                      <form action="/forums/{{$forum->id}}" method="POST">
                                          {{method_field('DELETE')}}
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-link" style="float: right; color: maroon"
                                          ><i class="fa fa-times"></i></button>

                                      </form>

                                  </div>
                                  <div class="col-md-2">

                                      <form action="/forums/{{$forum->id}}" method="POST">
                                          {{method_field('DELETE')}}
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <button type="submit" class="btn btn-link" style="float: right; color: maroon"
                                          ><i class="fa fa-times"></i></button>

                                      </form>

                                  </div>

                              </div>


                          </div>
                      @endif
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <a id="activity-desc" class="list-group-item list-group-item-action"
                             href="{{url('forums/'.$forum->id.'/live')}}">

                              <div class="activity-title"><p id="titleHead{{$forum->id}}">{{$forum->title}}</p>
                              </div>
                              <div class="activity-description"><p id="descPara{{$forum->id}}">{{$forum->desc}}</p>
                              </div>
                          </a>
                      </div>
                  </div>
                  <div>

                  </div>
              </div>
         </div>
     </div>
</div>