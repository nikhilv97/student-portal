@extends('layouts.app')


@section('content')

    <div class="container-fluid">
        <div class="row" >
            <div class="col-md-10 col-md-offset-1">
                {{--<input id="searchTextField" class="form-control" type="text">--}}
                {{--<button id="tt" class="btn btn-danger"></button>--}}
                <form class="navbar-form {{ $errors->has('filter') || $errors->has('query') ? ' has-error' : '' }}"
                      role="search" method="get" action={{url('/activities/search')}}>
                    <div class="input-group add-on">
                        <input class="form-control" placeholder="Search" name="query" id="query"
                               type="text"
                               value="{{isset($query) == null? "" : $query}}">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit" style="background: lightgray">
                                <i
                                        class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1 ">

                <div class="row " style="position: fixed; left: 70%; height: 40px; overflow-y: hidden">


                </div>

                <div class="row" style="margin-top: 80px">
                    @include('forum.forumList')
                </div>




            </div>
            <div class="background-image"></div>
        </div>
    </div>


@endsection

@section('footer')
    @include('layouts.footer')
    <script>
        $("#searchTextField").on("change paste keyup", function() {
            alert($(this).val());
        });
    </script>
@endsection

