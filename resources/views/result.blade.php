@extends('layouts.app')


@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(is_null($users) || empty($users) || $users->isEmpty())
                    @include('placeholders.noSearchResults')
                @else
                    @include('users.userList')
                @endif

            </div>
            <div class="background-image"></div>
        </div>
    </div>


@endsection

@section('footer')
    @include('layouts.footer')
@endsection