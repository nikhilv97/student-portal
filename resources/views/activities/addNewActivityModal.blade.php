<div class="modal fade col-xs-8 col-md-12" id="add_activity_modal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h3 style="text-align: center" class="modal-title" id="myModalLabel">Add Activity</h3>
            </div>
            <div class="modal-body">
                <form id="addActivityForm" class="form-horizontal" role="form" method="POST"
                      action="{{ url('/activity') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title:</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title"
                                   placeholder="Title of activity" value="{{old('title')}}"
                                   autofocus required>

                            <div id="titleError"></div>
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-4 control-label">Description:</label>

                        <div class="col-md-6">
                                        <textarea id="description" style="max-width: 100%" class="form-control"
                                                  name="desc"
                                                  placeholder="Description of activity">{{old('desc')}}</textarea>

                            <div id="descError"></div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                        <label for="link" class="col-md-4 control-label">Link to Repository:</label>

                        <div class="col-md-6">
                            <input id="link" type="text" class="form-control" name="external_link"
                                   placeholder="Link to activitied repository" value="{{old('link')}}">

                            <div id="linkError"></div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('external_link') ? ' has-error' : '' }}">
                        <label for="externalLink" class="col-md-4 control-label">Link to Application:</label>

                        <div class="col-md-6">
                            <input id="externalLink" type="text" class="form-control" name="link"
                                   placeholder="link like website/playstore to showcase your work" value="{{old('external_link')}}">

                            <div id="externalLinkError"></div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="type" class="col-md-4 control-label">Type:</label>
                        <div class="col-md-6">
                            <select id="type" class="form-control custom-select" name="type" required>
                                <option value="">Select Type</option>
                                <option value="competition"
                                        selected="{{ old('type') === 'competition' ? 'selected' : '' }}">Competition
                                </option>
                                <option value="seminar" selected="{{ old('type') === 'seminar' ? 'selected' : '' }}">
                                    Seminar
                                </option>
                                <option value="co_curricular"
                                        selected="{{ old('type') === 'co_curricular' ? 'selected' : '' }}">Cocurricular
                                </option>
                                <option value="guest_lecture"
                                        selected="{{ old('type') === 'guest_lecture' ? 'selected' : '' }}">Guest Lecture
                                </option>
                                <option value="certification"
                                        selected="{{ old('type') === 'certification' ? 'selected' : '' }}">Certification
                                </option>
                                <option value="training" selected="{{ old('type') === 'training' ? 'selected' : '' }}">
                                    Training
                                </option>
                                <option value="workshop" selected="{{ old('type') === 'workshop' ? 'selected' : '' }}">
                                    Workshop
                                </option>
                                <option value="project" selected="{{ old('type') === 'project' ? 'selected' : '' }}">
                                    Project
                                </option>
                                <option value="volunteer" selected="{{ old('type') === 'volunteer' ? 'selected' : '' }}">
                                    Volunteer
                                </option>
                                <option value="publication" selected="{{ old('type') === 'publication' ? 'selected' : '' }}">
                                    Publication
                                </option>
                                <option value="other" selected="{{ old('type') === 'other' ? 'selected' : '' }}">Other
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                        <label for="start" class="col-md-4 control-label">Start Date :</label>
                        <div class="col-md-6">
                            <input id="start" type="date" name="start" class="form-control" value="{{old('start')}}"
                                   required/>
                            <div id="startDateError"></div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('to') ? ' has-error' : '' }}">

                        <div class="col-md-6 col-md-offset-5">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input id="activity_ongoing" type="checkbox" class="form-check-input">
                                    Ongoing
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                        <label for="end" class="col-md-4 control-label">End Date :</label>
                        <div class="col-md-6">
                            <input id="end" type="date" name="end" class="form-control" value="{{old('end')}}"/>
                            <div id="endDateError"></div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                        <label for="select" class="col-md-4 control-label">Tags</label>
                        <div class="col-md-6">
                            <select id="select" name="tags[]" class="js-example-basic-multiple js-states form-control" required multiple="multiple">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->slug}}">{{$tag->value}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('tags'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" id="submitActivity" onclick="addActivity()" class="btn btn-primary pull-right">
                                Save
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {
        $('.js-example-basic-multiple').select2({
            placeholder: "Add tags to make them searchable",
            allowClear: true,
            width: '66%'
        });
    });

    function addActivity() {
        $('#submitActivity').button('loading');

        $('#addActivityForm').submit(function (event) {
//            event.preventDefault();

            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    $('#submitActivity').button('reset');

                    console.log(response);
                },

                failure: function (a, b, c) {
                    $('#submitActivity').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })
                .done(function (response) {
                    $('#submitActivity').button('reset');
                    $('#add_activity_modal').modal('hide');

                    var nodes = $('#activitiesListContainer > ul').length;

                    if (nodes === 0) {
                        $('#activitiesListContainer').append(response);
                    } else {
                        $('#activityList').prepend(response);
                    }
                })

                .fail(function (jqXhr, textStatus) {
                    $('#submitActivity').button('reset');

                    var errorJson = jQuery.parseJSON(jqXhr.responseText.trim());
                    console.log(errorJson);

                    if (errorJson.hasOwnProperty('title')) {
                        if ($('#titleError').is(':empty')) {
                            $('#titleError').append(errorElement(errorJson.title[0]));
                        }
                    } else {
                        $('#titleError').empty();
                    }

                    if (errorJson.hasOwnProperty('desc')) {
                        if ($('#descError').is(':empty')) {
                            $('#descError').append(errorElement(errorJson.desc[0]));
                        }
                    } else {
                        $('#descError').empty();
                    }

                    if (errorJson.hasOwnProperty('start')) {
                        if ($('#startDateError').is(':empty')) {
                            $('#startDateError').append(errorElement(errorJson.start[0]));
                        }
                    } else {
                        $('#startDateError').empty();
                    }

                    if (errorJson.hasOwnProperty('end')) {
                        if ($('#endDateError').is(':empty')) {
                            $('#endDateError').append(errorElement(errorJson.end[0]));
                        }
                    } else {
                        $('#endDateError').empty();
                    }

                    if (errorJson.hasOwnProperty('link')) {
                        if ($('#linkError').is(':empty')) {
                            $('#linkError').append(errorElement(errorJson.link[0]));
                        }
                    } else {
                        $('#linkError').empty();
                    }

                    if (errorJson.hasOwnProperty('external_link')) {
                        if ($('#externalLinkError').is(':empty')) {
                            $('#externalLinkError').append(errorElement(errorJson.external_link[0]));
                        }
                    } else {
                        $('#externalLinkError').empty();
                    }
                })

        });
    }

    $('#activity_ongoing').change(function () {
        if (!this.checked) {
            $('#end').prop('disabled', '');
        } else {
            $('#end').prop('disabled', 'disabled');
        }
    });



</script>
