<form class="form-horizontal" role="form" method="POST"
      action="{{ url('/activity') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="title" class="col-md-4 control-label">Title:</label>

        <div class="col-md-6">
            <input id="title" type="text" class="form-control" name="title"
                   placeholder="Title of activity"
                   autofocus required>

            @if ($errors->has('title'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                                </span>
            @endif
        </div>
    </div>


    <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
        <label for="description" class="col-md-4 control-label">Description:</label>

        <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="desc"
                                          placeholder="Description of activity" required></textarea>

            @if ($errors->has('desc'))
                <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="type" class="col-md-4 control-label">Type:</label>
        <div class="col-md-6">
            <select class="form-control custom-select" name="type" required>
                <option value="">Select Type</option>
                <option value="competition">Competition</option>
                <option value="seminar">Seminar</option>
                <option value="co_curricular">Cocurricular</option>
                <option value="guest_lecture">Guest Lecture</option>
                <option value="certification">Certification</option>
                <option value="training">Training</option>
                <option value="workshop">Workshop</option>
                <option value="project">Project</option>
                <option value="other">Other</option>
            </select>
        </div>
    </div>
    <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
        <label for="start" class="col-md-4 control-label">Start Date :</label>
        <div class="col-md-6">
            <input id="start" type="date" name="start" class="form-control"
                   required/>
            @if ($errors->has('start'))
                <span class="help-block">
                                        <strong>{{ $errors->first('start') }}</strong>
                                                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
        <label for="end" class="col-md-4 control-label">End Date :</label>
        <div class="col-md-6">
            <input id="end" type="date" name="end" class="form-control"/>
            @if ($errors->has('end'))
                <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                                </span>
            @endif
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Save
            </button>
        </div>
    </div>
</form>