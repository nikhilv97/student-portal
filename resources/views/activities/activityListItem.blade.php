@inject('userService', 'ConnectInn\Services\UserService')

@php
    use ConnectInn\User;
    $owner = $activity->owner();
$members = $activity->members();
$ownerWorkingAs = $activity->users()->where('email', $owner->email)->value('working_as');



@endphp


<div class="panel panel-default activitybox">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <img src="{{$owner->profile_pic_url}}" height="50" width="50" alt="pic"
                             class="img-circle">@php
                                @endphp
                        <a class="username" href="/user/{{ $owner->id}}/timeline">{{ $owner->name}} </a><em
                                style="color: darkgray;">{{is_null($ownerWorkingAs) ? '' : '(' . title_case($ownerWorkingAs) . ')'}}</em>
                        <div class="col-md-offset-1">
                            @if(is_null($activity->end))
                                ( Ongoing )
                            @else
                        ( finished )
                            @endif

                        </div>


                        <div>
                            <hr style="border: 1px solid darkgray">
                        </div>
                    </div>

                    @if($activity->users->contains(\Illuminate\Support\Facades\Auth::id()))
                        <div class="col-md-4">
                            <div class="row pull-right">
                                <div class="col-md-2">

                                    <button type="submit" onclick="activityToggle{{$activity->id}}()"
                                            class="btn btn-link"
                                            style="float: right; color: midnightblue"
                                    ><i class="fa fa-pencil-square-o"></i></button>

                                </div>
                                <div class="col-md-2">

                                    <form action="/activity/{{$activity->id}}" method="POST">
                                        {{method_field('DELETE')}}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-link" style="float: right; color: maroon"
                                        ><i class="fa fa-times"></i></button>

                                    </form>

                                </div>

                            </div>


                        </div>
                    @endif
                </div>

                @if(count($members) != 0)
                    <div class="row">
                        <div class="col-md-offset-1">
                            <div class="dropdown">
                                Members <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-expanded="false">
                                    <i class="fa fa-caret-down" aria-hidden="true" style="font-size: 1.5em"></i>
                                </a>


                                <ul class="dropdown-menu" role="menu">
                                    <div class="container-fluid">
                                        <br>
                                        @foreach($members as $member)
                                            <li>
                                                <img src="{{$member->profile_pic_url}}" height="30" width="30" alt="pic"
                                                     class="img-circle"><a
                                                        href="/user/{{$member->id}}/timeline">{{$member->name}} </a><em
                                                        style="color: darkgray">({{$userService->getMemberRole($member, $activity)}}
                                                    )</em>
                                            </li>
                                            <br>
                                        @endforeach
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif


                <div class="row">
                    <div class="col-md-12">
                        <a id="activity-desc" class="list-group-item list-group-item-action"
                           href="{{url('activity/'.$activity->id)}}">

                            <div class="activity-title"><p id="titleHead{{$activity->id}}">{{$activity->title}}</p>
                            </div>
                            <div class="activity-description"><p id="descPara{{$activity->id}}">{{$activity->desc}}</p>
                            </div>
                        </a>
                    </div>
                </div>

                @if(!is_null($activity->link))
                    <div class="row">
                        <a role="button" class="btn btn-default pull-right" style="margin-right: 50px"
                           href="{{URL::to($activity->link)}}" target="_blank">See my work</a>
                    </div>
                @endif


            </div>
        </div>
    </div>
</div>



<script>
    jQuery(document).ready(function () {
        $.fn.editable.defaults.mode = 'inline';
        $.fn.editable.defaults.ajaxOptions = {type: 'PATCH'};

        $('#titleHead' + '{{$activity->id}}').editable({
            type: 'text',
            title: 'This a test',
            url: 'activity/{{$activity->id}}',
            params: {_token: "{{csrf_token()}}"},
            disabled: true,
            send: 'always',
            name: 'title',
            success: function (data) {
                console.log(data);
            }


        });

        $('#descPara' + '{{$activity->id}}').editable({
            type: 'textarea',
            title: 'This a test',
            url: 'activity/{{$activity->id}}',
            params: {_token: "{{csrf_token()}}"},
            disabled: true,
            send: 'always',
            name: 'desc',
            success: function (data) {
                console.log(data);
            }


        });
    });

    function activityToggle{{$activity->id}}() {
        $('#titleHead' + '{{$activity->id}}').editable('toggleDisabled');
        $('#descPara' + '{{$activity->id}}').editable('toggleDisabled');

    }


</script>

