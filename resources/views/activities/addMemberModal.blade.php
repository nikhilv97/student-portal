<div id="modal" class="modal fade col-md-12 col-xs-12" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center">
                    Add Email
                </h3>
            </div>
            <div class="modal-body">
                <form id="form" action="{{url('/activity/'.$activity->id.'/users/attach')}}" class="form-horizontal"
                      method="post">
                    {{csrf_field()}}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address : </label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="workingAs" class="col-md-4 control-label">Working As :</label>

                        <div class="col-md-6">
                            <input id="workingAs" type="text" class="form-control" name="workingAs" value="{{ old('workingAs') }}">

                            @if ($errors->has('workingAs'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('workingAs') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" id="post" onclick="submitStory()" class="btn btn-primary">
                                Invite
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script>
    function submitStory() {
        $('#post').button('loading');

        $('#form').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    $('#post').button('reset');
                    $('#modal').modal('hide');
                    location.reload();
                    console.log(status);
                },

                failure: function (a, b, c) {
                    $('#post').button('reset');
//                    alert('something went wrong');
                    console.log(a);
                }
            })

        });




    }

</script>
