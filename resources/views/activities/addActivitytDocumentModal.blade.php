<div id="add_activity_document_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Document
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="form" action="{{url('/activity/'.$activity->id.'/documents')}}" class="form-horizontal"
                                  method="post" style="max-height: 500px" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="title" class="col-md-4 control-label">Title: </label>

                                    <div class="col-md-6">
                                        <input id="title" type="text" placeholder="Title " class="form-control" name="title" value="{{ old('title') }}"
                                               autofocus>

                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description" class="col-md-4 control-label">Description:</label>

                                    <div class="col-md-6">
                                        <textarea id="description" style="max-width: 100%;" class="form-control"
                                                  name="description"
                                                  placeholder="Description " >{{old('description')}}</textarea>

                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div id="imageDiv" class="caption" >


                                    <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                        <label for="file" class="col-md-4 control-label">File:</label>

                                        <div class="col-md-6 col-xs-6">
                                            <input type="file" class="form-control" name="file" required>

                                            @if ($errors->has('file'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="post"  class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    function submitActivityDocument() {
        $('#post').button('loading');

        $('#form').submit(function (event) {

            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method : $(this).attr('method'),
                data : $(this).serialize(),
                success : function (response, status, jq) {
                    $('#post').button('reset');
                    $('#story_modal').modal('hide');
                    location.reload();
                },

                failure: function (a, b, c) {
                    $('#post').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })

        });




    }

</script>