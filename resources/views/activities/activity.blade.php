@inject('userService', 'ConnectInn\Services\UserService')

@php
    $milestones = $activity->milestones;

    $stories = $milestones->filter(function ($value ,$key){
    return $value->type === 'story';
    });

$tasks = $milestones->filter(function ($value ,$key){
    return $value->type === 'task';
    });

if(!isset($documents)){
$documents = $activity->documents;
    /*dd($documents);*/
}

$activityRequest = $activity->activityRequests;
$activityRequest->load('user');
//dd($activityRequest);
@endphp


@extends('layouts.app')



@section('content')

    <div>

        @include('activities.addMemberModal')

    </div>



    <div>

        @include('activities.milestones.addTaskModal')

    </div>



    <div>

        @include('activities.milestones.addStoryModal')

    </div>

    <div>

        @include('activities.addActivitytDocumentModal')

    </div>

    <div class="container-fluid">
        <div class="row">


            <div class="col-md-12">

                <div class="col-md-5">

                    <div class="row">
                        <div class="jumbotron" style="background: #b93838; max-height: 1000px">

                            <div>
                                <h3 align="center" style="color: white">{{$activity->title}}</h3>
                            </div>


                            <p class="col-md-12"
                               style="text-align: center;color: lightgray; font-weight: 400;font-style: italic">{{$activity->start}}
                                to
                                @if(!(is_null($activity->end)))
                                    {{$activity->end}}
                                @else
                                    present
                                @endif
                            </p>

                            <p style="text-align: center;color: white; font-weight: 400">{{$activity->desc}}


                            @if(!($activity->users->contains(Auth::user())))
                                <div>
                                    <form action="{{url('activity/'.$activity->id.'/request')}}" class="form-horizontal"
                                          method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <div class="col-md-12 col-md-offset-5">
                                                @if(!($activity->activityRequests->contains('user_id',(Auth::user()->id))))
                                                    <button type="submit" class="btn btn-primary" style="background:maroon; border-color: maroon ">
                                                        Connect
                                                    </button>
                                                @else
                                                    <button class="btn-btn-primary" disabled>
                                                        Connect Request Sent
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                            @if($activity->users->contains(\Illuminate\Support\Facades\Auth::user()->id) )
                                <div class="row">
                                    @if(!(is_null($activity->link)))
                                        <a role="button" class="btn btn-default pull-right" style="margin-right: 50px; background:maroon; border-color: maroon;color: white"
                                           href="{{URL::to($activity->link)}}" target="_blank">See work</a>

                                        @endif

                                        @if(!is_null($activity->external_link))
                                            <a role="button" class="btn btn-default pull-left" style="margin-left: 50px; background:maroon; border-color: maroon; color: white" href="{{URL::to($activity->external_link)}}" target="_blank">Go to work repository</a>

                                        @endif
                                </div>

                            @endif
                        </div>
                    </div>


                    <br>

                    <br>

                    @if($userService->isLoggedIn($activity->owner()) && $activity->activityRequests->isNotEmpty())
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 align="center">Connection Requests
                                    </h2>
                                </div>

                                <div class="panel-body">
                                    <ul class="list-group">
                                        @foreach($activityRequest as $req)

                                            <div>
                                                <li id="activityRequest{{$req->id}}" class="list-group-item"><a
                                                            href="{{ url('/user/'.  $req->user->id  . '/timeline') }}">{{$req->user->name}}
                                                    </a>


                                                    <i class="fa fa-check-circle pull-right"
                                                       onclick="accept{{$req->id}}()" aria-hidden="true"
                                                       style="color: green; font-size: 1.5em"></i>

                                                    <i class="fa fa-times-circle pull-right"
                                                       onclick="reject{{$req->id}}()" aria-hidden="true"
                                                       style="font-size: 1.5em; color: red"></i>

                                                </li>
                                            </div>


                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            <br>
                        </div>
                    @endif

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="heading-box">
                                <h3 align="center">Members

                                    @if($userService->isLoggedIn($activity->owner()))

                                        <button class="btn btn-default pull-right" data-toggle="modal"
                                                data-target="#modal"
                                                data-backdrop="false" style="border: none">

                                            <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                               aria-hidden="true"></i>


                                        </button>
                                    @endif
                                </h3>


                            </div>
                            <div class="panel-body">
                                @foreach($activity->users as $user)
                                    @include('users.member')
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-7">
                    <div class="panel panel-default">
                        <div class="heading-box"><h3 align="center">Milestones</h3></div>
                        <div class="panel-body">
                            <div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active col-md-6 col-xs-6"><a href="#story"
                                                                                                aria-controls="story"
                                                                                                role="tab"
                                                                                                data-toggle="tab"><i
                                                    class="fa fa-envelope" aria-hidden="true"></i> Stories
                                            @if($userService->isLoggedIn($activity->owner()))

                                                <button class="btn btn-default pull-right" data-toggle="modal"
                                                        data-target="#story_modal"
                                                        data-backdrop="false" style="border: none; padding: 0">

                                                    <i class="fa fa-plus " style="font-size:1em; color: #444444"
                                                       aria-hidden="true"></i>


                                                </button>
                                            @endif
                                        </a></li>
                                    <li role="presentation" class="col-md-6 col-xs-6"><a href="#task"
                                                                                         aria-controls="task"
                                                                                         role="tab"
                                                                                         data-toggle="tab"><i
                                                    class="fa fa-tasks" aria-hidden="true"></i> Tasks
                                            @if($userService->isLoggedIn($activity->owner()))

                                                <button class="btn btn-default pull-right" data-toggle="modal"
                                                        data-target="#task_modal"
                                                        data-backdrop="false" style="border: none; padding: 0">

                                                    <i class="fa fa-plus "
                                                       style="font-size:1em; color: #444444; "
                                                       aria-hidden="true"></i>


                                                </button>
                                            @endif
                                        </a>


                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="story">
                                        @if(!isset($stories) || is_null($stories) || count($stories) === 0)
                                            @include('placeholders.noStories')
                                        @else
                                            @foreach($stories as $story)
                                                @include('activities.milestones.story')
                                            @endforeach
                                        @endif
                                        {{--                                @each('activities.milestones.story', $stories , 'story', 'placeholders.noStories')--}}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="task">
                                        @if(!isset($tasks) || is_null($tasks) || count($tasks) === 0)
                                            @include('placeholders.noTasks')
                                        @else
                                            @foreach($tasks as $task)
                                                @include('activities.milestones.task')
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 align="center"> Documents/Certificates
                                @if ($activity->users->contains(\Illuminate\Support\Facades\Auth::user()->id)))
                                <button class="btn btn-default pull-right" data-toggle="modal"
                                        data-target="#add_activity_document_modal"
                                        data-backdrop="false" style="border: none">

                                    <i class="fa fa-plus" style="font-size:1em; color: #444444"
                                       aria-hidden="true"></i>


                                </button>
                                @endif
                            </h4>
                        </div>

                        <div class="panel-body">

                            @if(count($documents) === 0)
                                @include('placeholders.noDocuments')
                            @else
                                @include('users.documnets.documentList')
                            @endif

                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>


    <script>
        @foreach($activityRequest as $req)
        function accept{{$req->id}}() {
            console.log('clicked');
            $.ajax('/activity-request/{{$req->id}}/accept', {
                method: 'POST',
                data: {
                    _token: '{{csrf_token()}}'
                }
            })
                .done(function (response) {
                    console.log(response);
                    location.reload();

                })

                .fail(function (jqXhr, textStatus) {
                    console.log(jqXhr);
                    alert('Something went worng, please try again');
                })
        }

        function reject{{$req->id}}() {
            $.ajax('/activity-request/{{$req->id}}/reject', {
                method: 'POST',
                data: {
                    _token: '{{csrf_token()}}'
                }
            })
                .done(function (response) {
                    console.log(response)
                    location.reload()

                })

                .fail(function (jqXhr, textStatus) {
                    console.log(jqXhr)
                })
        }
        @endforeach
    </script>

@endsection
