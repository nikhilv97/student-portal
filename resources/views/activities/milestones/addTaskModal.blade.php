<div id="task_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Task
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addTaskForm" action="{{url('/activity/'.$activity->id.'/milestones/task')}}"
                                  class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Title</label>

                                    <div class="col-md-6">
                                        <input id="title" placeholder="Title of the task" type="text"
                                               class="form-control" name="title" value="{{ old('title') }}"
                                               required autofocus>

                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Select Member</label>

                                    <div class="col-md-6">
                                        {{-- <input id="email" type="email" placeholder="Email of the member" class="form-control" name="email" value="{{ old('email') }}"
                                                required autofocus>--}}
                                        <select id="email" class="form-control custom-select" name="email" required>
                                            <option value="" style="font-style: italic; background: darkgray">Select
                                                Member
                                            </option>

                                            @foreach($activity->users as $member)
                                                <option value="{{$member->email}}">{{$member->name}}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="descriptionription" class="col-md-4 control-label">Description:</label>

                                    <div class="col-md-6">
                                        <textarea id="description" style="max-width: 100%" class="form-control"
                                                  name="description"
                                                  placeholder="Description of activity"
                                                  required>{{old('description')}}</textarea>

                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" onclick="submitTask()" id="postTask"
                                                class="btn btn-primary">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    function submitTask() {
        $('#postTask').button('loading');

        $('#addTaskForm').off('submit').submit(function (event) {
            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (response, status, jq) {
                    $('#postTask').button('reset');
                    $('#task_modal').modal('hide');
                    console.log(response);


                    $('#task').prepend(response);
                },

                failure: function (a, b, c) {
                    $('#postTask').button('reset');
                    alert('something went wrong');
                    console.log(a);
                }
            })

        });


    }

</script>
