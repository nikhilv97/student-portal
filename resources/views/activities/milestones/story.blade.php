<li class="list-group-item">
    <div class="container-fluid">
        <div class="row">
            <i class="fa fa-envelope"></i>
            <strong>{{ $story->title }}</strong>
        </div>
        <br>
        <div class="row">
            {{ $story->description }}
        </div>
    </div>
</li>