<li class="list-group-item">
    <div class="container-fluid">
        <div class="row">
            <i class="fa fa-tasks"></i>
            <strong>{{ $task->title }}</strong>
        </div>

        <strong style="color:darkgray; font-style: italic"><strong>Assigned to
                : </strong>{{\ConnectInn\User::find($task->assigned_user_id)->name}}</strong>

        <br>
        {{--<div class="row" >--}}
        {{--<strong>Status: </strong>--}}
        {{--<p id="status" style="color:darkred;">{{ title_case($task->status) }}</p>--}}
        {{--</div>--}}
        <br>
        <div class="row">
            {{ $task->description }}
        </div>
        <br>
        <div id="buttonsHolder{{ $task->id }}" class="pull-right">
            @if($task->status === 'created')
                <button id="start" onclick="changeStatus{{$task->id}}('started')" class="btn btn-default">Start
                </button>

            @elseif($task->status === 'started')
                <button id="finish" onclick="changeStatus{{$task->id}}('finished')" class="btn btn-default">
                    Finish
                </button>

            @elseif($task->status === 'finished')
                <button id="accept" onclick="changeStatus{{$task->id}}('accepted')" class="btn btn-default">
                    Accept
                </button>
                <button id="reject" onclick="changeStatus{{$task->id}}('rejected')" class="btn btn-default">
                    Reject
                </button>
            @elseif($task->status === 'rejected')
                <button id="restart" onclick="changeStatus{{$task->id}}('started')" class="btn btn-default">
                    Restart
                </button>

            @else
            @endif

        </div>
    </div>
</li>

<script>
    function changeStatus{{$task->id}}(toStatus) {
        console.log(toStatus);

        $.ajax('/milestones/' + '{{$task->id}}', {
            method: "PATCH",
            data: {
                attribute: 'status',
                value: toStatus,
                _token: '{{csrf_token()}}'
            },
            success: function (response, status, jqXHR) {
                console.log(response, status, jqXHR);

                $('#buttonsHolder<?php echo e($task->id); ?>').empty();

                var result = response.toString().trim();
                console.log(result);

                if (result === 'started') {

                    $('#buttonsHolder<?php echo e($task->id); ?>').append("<button onclick=" + "changeStatus{{$task->id}}('finished')" + " class='btn btn-default'>Finish</button>");

                } else if (result === 'finished') {

                    $('#buttonsHolder<?php echo e($task->id); ?>').append("<button onclick=" + "changeStatus{{$task->id}}('accepted')" + " class='btn btn-default'>Accept</button>");
                    $('#buttonsHolder<?php echo e($task->id); ?>').append("<button onclick=" + "changeStatus{{$task->id}}('rejected')" + " class='btn btn-default'>Reject</button>");

                } else if (result === 'rejected') {

                    $('#buttonsHolder<?php echo e($task->id); ?>').append("<button onclick=" + "changeStatus{{$task->id}}('started')" + " class='btn btn-default'>Restart</button>");

                }

            },

            error: function (a, b, c) {
                console.log("error");
                console.log(a, b, c);
            }
        });
    }
</script>