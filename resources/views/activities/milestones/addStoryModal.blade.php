<div id="story_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title" style="text-align: center">
                                Add Story
                            </h3>
                        </div>
                        <div class="modal-body">
                            <form id="addStoryform" action="{{url('/activity/'.$activity->id.'/milestones/story')}}" class="form-horizontal"
                                  method="post">
                                {{csrf_field()}}

                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Title</label>

                                    <div class="col-md-6">
                                        <input id="title" type="text" placeholder="Title of activity" class="form-control" name="title" value="{{ old('title') }}"
                                               required autofocus>

                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description" class="col-md-4 control-label">Description:</label>

                                    <div class="col-md-6">
                                        <textarea id="description" style="max-width: 100%;" class="form-control"
                                                  name="description"
                                                  placeholder="Description of activity" required>{{old('description')}}</textarea>

                                        @if ($errors->has('description'))
                                            <span class="text-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" id="postStory" onclick="submitStory()" class="btn btn-primary">
                                            Post
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    function submitStory() {

        $('#postStory').button('loading');

        $('#addStoryform').off('submit').submit(function (event) {
            event.preventDefault();

           $.ajax($(this).attr('action'), {
               method : $(this).attr('method'),
               data : $(this).serialize(),
               success : function (response, status, jq) {
                   $('#postStory').button('reset');
                   $('#story_modal').modal('hide');
//                   location.reload();
                   console.log(response);


                   $('#story').prepend(response);


                   
                   console.log(response);
               },

               failure: function (a, b, c) {
                   $('#postStory').button('reset');
                   alert('something went wrong');
                   console.log(a);
               }
           })

        });
    }

</script>