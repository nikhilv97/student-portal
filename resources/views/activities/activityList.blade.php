@php
    if(!isset($activities)){
    $activities = $user->activities;
    }
@endphp


<ul id="activityList" class="list-group">
            @each('activities.activityListItem', $activities, 'activity', 'placeholders.noActivities')
</ul>