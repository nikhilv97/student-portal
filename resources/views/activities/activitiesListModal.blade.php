<div id="activity_list_modal" class="modal fade col-md-12 col-xs-12" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="text-align: center">
                    Invite to Activity
                </h3>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    @foreach(Auth::user()->activities as $key=>$activity)
                       @if(!($activity->users->contains($user->id)))
                    <li class="list-group-item"  style="cursor:pointer" onclick="invite({{$activity->id}})">{{$activity->title}}
                        <i class="fa fa-spinner fa-spin pull-right" id="loaderDiv{{$activity->id}}" style="visibility: hidden" aria-hidden="true"></i></li>
                           @else
                            <li class="list-group-item" >{{$activity->title}} <strong class="pull-right">Added</strong></li>


                        @endif
                        @endforeach

                </ul>
            </div>
        </div>

    </div>
</div>

<script>
   function invite(activityId) {
       $("#loaderDiv" + activityId).prop('style', 'visibility: visible');
       console.log(activityId);

       $.ajax('/activity/' + activityId + '/users/attach', {
           method : "POST",
           data : {
               email: '{{$user->email}}',
               _token: '{{csrf_token()}}'
           },
           success: function (response , status ,jqXHR) {
               console.log("success");
               $("#loaderDiv" + activityId).prop('style', 'visibility: hidden');
               $('#activity_list_modal').modal('hide');
               alert('Mail Sent! Your Invitation to connect was sent.');

           },
           error : function (a,b,c) {
               console.log("error");
               console.log(a);
               $("#loaderDiv" + activityId).prop('style', 'visibility: hidden');
               $('#activity_list_modal').modal('hide');
               alert('Oops! There was some error');

           }
       });

   }





</script>
