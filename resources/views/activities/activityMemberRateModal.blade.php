<style>
    div.stars {
        width: 270px;
        display: inline-block;
    }

    div.starsSmall {
        width: 10px;
        display: inline;
    }

    input.star {
        display: none;
    }

    label.star {
        float: right;
        padding: 10px;
        font-size: 36px;
        color: #444;
        transition: all .2s;
    }

    input.star:checked ~ label.star:before {
        content: '\f005';
        color: #FD4;
        transition: all .25s;
    }

    input.star-5:checked ~ label.star:before {
        color: #FE7;
        text-shadow: 0 0 20px #952;
    }

    input.star-1:checked ~ label.star:before {
        color: #F62;
    }

    label.star:hover {
        transform: rotate(-15deg) scale(1.3);
    }

    label.star:before {
        content: '\f006';
        font-family: FontAwesome;
    }

</style>
<div id="rate_modal" class="modal fade" role="dialog" style="padding-right: 50em; padding-top: 10em ">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="modal-content" style=" height: 190px">
                        <div class="col-md-11" style="padding-top: 1em">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Rate Member</h4></div>
                        <br>
                        <hr>


                        <div class="col-md-11">
                            <div class="stars">
                                <form action="{{url('/activity/'.$activity->id.'/users/'.$user->id.'/rating')}}"
                                      method="post">
                                    {{csrf_field()}}
                                    <input class="star star-5" value="5" id="star-5" type="radio" name="rating"/>
                                    <label class="star star-5" for="star-5"></label>
                                    <input class="star star-4" id="star-4" value="4" type="radio" name="rating"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-3" id="star-3" value="3" type="radio" name="rating"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-2" id="star-2" value="2" type="radio" name="rating"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-1" id="star-1" value="1" type="radio" name="rating"/>
                                    <label class="star star-1" for="star-1"></label>
                                    <div class="pull-right"><input type="submit" value="Rate"
                                                                   class="btn btn-default pull-right">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
