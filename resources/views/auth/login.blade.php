<div class="container">
    <div class="row">
        <div class="col-md-10" style="padding-top: 2em">


            <form id="loginForm" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email"
                               value="{{old('email')}}"
                               required autofocus>

                        <div id="loginError">

                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <input type="hidden" value="login" name="type">

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember
                                Me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button id="loginSubmitButton" onclick="submitLoginForm()" type="submit"
                                class="btn btn-primary">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ url('/password/reset') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function submitLoginForm() {
        $('#loginSubmitButton').button('loading');
        $('#loginSubmitButton').button('reset');
        var errorElement = function (message) {
            return "<span class='help-block' style='color: red'><strong>" + message + "</strong></span>"
        };

        $('#loginForm').submit(function (event) {
            event.preventDefault();

            $.ajax($(this).attr('action'), {
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (response, status, jq) {
                    $('#loginSubmitButton').button('reset');
                },

                failure: function (a, b, c) {
                    $('#loginSubmitButton').button('reset');
                    alert('Something went wrong, please try again!');
                    console.log(a, b, c);
                }
            })
                .done(function (response) {
                    $('#loginSubmitButton').button('reset');
                    window.location.href = '/home';
                })
                .fail(function (jqXHR, textStatus) {
                    $('#loginSubmitButton').button('reset');
                    $('#loginError').empty();
                    $('#loginError').append(errorElement(jqXHR.responseJSON['email']));
                });

        });
    }
</script>

