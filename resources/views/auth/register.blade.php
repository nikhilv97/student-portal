<div class="container">
    <div class="row">
        <div class="col-md-10 " style="padding-top: 2em">

            <form id="signupForm" class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required
                               autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email"
                               value="{{ session('type') === 'signup' ? old('email') : '' }}"
                               required>

                        <div id="emailSignupError"></div>
                    </div>
                </div>

                <input type="hidden" value="signup" name="type">

                <div class="form-group{{ $errors->has('password')  ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        <div id="passwordSignupError"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button id="signupButton" type="submit" onclick="submitSignUpForm()" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function submitSignUpForm() {

        var errorElement = function (message) {
            return "<span class='help-block' style='color: red'><strong>" + message + "</strong></span>"
        };
        $('#signupButton').button('loading');
        $('#signupButton').button('reset');

        $('#signupForm').submit(function (event) {
            event.preventDefault();
            $.ajax($(this).attr('action'), {
                method: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (response, status, jq) {
                    $('#signupButton').button('reset');
                },

                failure: function (a, b, c) {
                    $('#loginSubmitButton').button('reset');
                    alert('Something went wrong, please try again!');
                    console.log(a, b, c);
                }
            })
                .done(function (response) {
                    $('#signupButton').button('reset');
                    window.location.href = '/home';
                })
                .fail(function (jqXHR, textStatus) {
                    //console.log(jqXHR);
                    var errorJson = jQuery.parseJSON(jqXHR.responseText.trim());
                    console.log('this is error json',errorJson);
                    if (errorJson.hasOwnProperty('email')) {

                        if ($('#emailSignupError').is(':empty')) {
                            console.log(errorElement(errorJson.email[0]));
                            $("#emailSignupError").append(errorElement(errorJson.email[0]));
                        }
                    } else {
                        $('#emailSignupError').empty();
                    }


                    if (errorJson.hasOwnProperty('password')) {
                        if ($('#passwordSignupError').is(':empty')) {
                            $('#passwordSignupError').append(errorElement(errorJson.password[0]));
                        }
                    } else {
                        $('#passwordSignupError').empty();
                    }

                    $('#signupButton').button('reset');
                });

        });
    }
</script>

