<style>
    html, body {
        height: 100%;
    }

    /*body {*/
        /*margin: 0;*/
        /*padding: 0;*/
        /*color: #B0BEC5;*/
    /*}*/

    .content {
        text-align: center;
        display: inline-block;
    }

    .error_title {
        width: 100%;
        font-weight: 100;
        font-size: 35px;
        color: #B0BEC5;
        margin-bottom: 40px;
    }
</style>
<div class="container-fluid" style="text-align: center">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="content">
                <div class="error_title">You have not created any Stories yet!</div>
                @if(\Illuminate\Support\Facades\Auth::id() == $activity->owner()->id)

                    <button class="btn btn-default" data-toggle="modal"
                            data-target="#story_modal"
                            data-backdrop="false">

                        <strong>Float your first Story</strong>


                    </button>
                @endif
            </div>
        </div>
    </div>
</div>