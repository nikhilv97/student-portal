<div id="noSkill">
    <style>
        html, body {
            height: 100%;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .error_title {
            width: 100%;
            font-weight: 100;
            font-size: 48px;
            color: #B0BEC5;
            margin-bottom: 450px;
        }
    </style>
    <div class="container-fluid" style="text-align: center">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="content">
                    <div class="error_title">No Skills added yet!</div>
                </div>
            </div>
        </div>
    </div>
</div>