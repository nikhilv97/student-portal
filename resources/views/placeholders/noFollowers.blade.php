@section('content')
    <style>
        html, body {
            height: 100%;
        }

        /*body {*/
            /*margin: 0;*/
            /*padding: 0;*/
            /*color: #B0BEC5;*/
        /*}*/

        .container {
            text-align: center;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .error_title {
            width: 100%;
            font-weight: 100;
            font-size: 48px;
            color: #B0BEC5;
            margin-bottom: 40px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="error_title" style="margin: 50px">No followers to display!</div>
            </div>
        </div>
    </div>
@endsection

