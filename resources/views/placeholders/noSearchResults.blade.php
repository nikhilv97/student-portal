<style>
    html, body {
        height: 100%;
    }

    /*body {*/
    /*margin: 0;*/
    /*padding: 0;*/
    /*color: #B0BEC5;*/
    /*}*/

    .content {
        text-align: center;
        display: inline-block;
    }

    .error_title {
        width: 100%;
        font-weight: 400;
        font-size: 48px;
        color: #128f76;
        margin-bottom: 450px;
    }
</style>
<div class="container-fluid" style="text-align: center">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="content">
                <div class="error_title">No such results found! Search with other parameters.</div>
            </div>
        </div>
    </div>
</div>