@php
    $requests = Auth::user()->activityRequests;
@endphp


@extends('layouts.app')

@section('content')

    <style>
        ::-webkit-scrollbar {
            display: none;
        }
    </style>


    <div class="container-fluid">

        <div class="col-md-12">
            <div class="row">


                <div class="col-md-3">

                    <div class="row">
                        <div class="profilebox">
                            <div class="text-center">
                                <img class="img-circle" src="{{Auth::user()->profile_pic_url}}" height="150px"
                                     width="150px">

                                <h4>{{Auth::user()->name}}</h4>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <form id="addArticleForm" action="{{url('/articles')}}" class="form-horizontal"
                                              method="post">
                                            {{csrf_field()}}


                                            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">

                                                <textarea id="text" placeholder="Share your thoughts with everyone..."
                                                          class="form-control" name="text"
                                                          style="height: 100px ; max-height: 200px; max-width: 100%"
                                                          required autofocus></textarea>

                                                @if ($errors->has('text'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                                @endif


                                                <div class="pull-right" style="margin-top: 1em">
                                                    <button type="submit" id="postArticle" class="btn btn-danger">
                                                        Add
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-md-9">
                     @include('articles.articleList')
                    {{--@include('activities.activityList')--}}

                    <nav aria-label="Page navigation" class="text-center">
                        <ul class="pagination col-xs-12 col-md-12 text-center">
                            <li>
                                <a href="{{$articles->previousPageUrl()}}" aria-label="Previous">
                                    <span aria-hidden="true">{{Lang::get('pagination.previous')}}</span>
                                </a>
                            </li>
                            @for($i=1; $i<=$articles->lastPage(); $i++)
                                <li class="{{$articles->currentPage() === $i ? "active" : ""}}">
                                    <a href="{{$articles->url($i)}}">{{$i}}</a>
                                </li>
                            @endfor
                            <li>
                                <a href="{{$articles->nextPageUrl()}}" aria-label="Next">
                                    <span aria-hidden="true">{{Lang::get('pagination.next')}}</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="background-image"></div>
            </div>

        </div>
    </div>

    <script>
        @foreach($requests as $req)
        function acceptRequest {{$req->id}}() {
            console.log('clicked');
            $.ajax('/activity-request/{{$req->id}}/accept', {
                method: 'POST',
                data: {
                    _token: '{{csrf_token()}}'
                }
            })
                .done(function (response) {
                    console.log(response);
                    location.reload();

                })

                .fail(function (jqXhr, textStatus) {
                    console.log(jqXhr);
                    alert('Something went worng, please try again');
                })
        }

        function rejectRequest {{$req->id}}() {
            $.ajax('/activity-request/{{$req->id}}/reject', {
                method: 'POST',
                data: {
                    _token: '{{csrf_token()}}'
                }
            })
                .done(function (response) {
                    console.log(response)

                })

                .fail(function (jqXhr, textStatus) {
                    console.log(jqXhr)
                })
        }
        @endforeach
    </script>
@endsection

@section('footer')
    @include('layouts.footer')
@endsection

